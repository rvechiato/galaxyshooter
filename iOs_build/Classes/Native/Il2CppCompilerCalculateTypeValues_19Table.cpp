﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.Mesh
struct Mesh_t3648964284;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>
struct Dictionary_2_t125631422;
// System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture>
struct HashSet_1_t673836907;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.PostProcessing.PostProcessingProfile
struct PostProcessingProfile_t724195375;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.PostProcessing.MaterialFactory
struct MaterialFactory_t2445948724;
// UnityEngine.PostProcessing.RenderTextureFactory
struct RenderTextureFactory_t1946967824;
// UnityEngine.PostProcessing.PostProcessingContext
struct PostProcessingContext_t2014408948;
// System.Char[]
struct CharU5BU5D_t3528271667;
// UnityEngine.PostProcessing.ColorGradingCurve
struct ColorGradingCurve_t2000571184;
// System.Void
struct Void_t1185182177;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.PostProcessing.BuiltinDebugViewsModel
struct BuiltinDebugViewsModel_t1462618840;
// UnityEngine.PostProcessing.FogModel
struct FogModel_t3620688749;
// UnityEngine.PostProcessing.AntialiasingModel
struct AntialiasingModel_t1521139388;
// UnityEngine.PostProcessing.AmbientOcclusionModel
struct AmbientOcclusionModel_t389471066;
// UnityEngine.PostProcessing.ScreenSpaceReflectionModel
struct ScreenSpaceReflectionModel_t3026344732;
// UnityEngine.PostProcessing.DepthOfFieldModel
struct DepthOfFieldModel_t514067330;
// UnityEngine.PostProcessing.MotionBlurModel
struct MotionBlurModel_t3080286123;
// UnityEngine.PostProcessing.EyeAdaptationModel
struct EyeAdaptationModel_t242823912;
// UnityEngine.PostProcessing.BloomModel
struct BloomModel_t2099727860;
// UnityEngine.PostProcessing.ColorGradingModel
struct ColorGradingModel_t1448048181;
// UnityEngine.PostProcessing.UserLutModel
struct UserLutModel_t1670108080;
// UnityEngine.PostProcessing.ChromaticAberrationModel
struct ChromaticAberrationModel_t3963399853;
// UnityEngine.PostProcessing.GrainModel
struct GrainModel_t1152882488;
// UnityEngine.PostProcessing.VignetteModel
struct VignetteModel_t2845517177;
// UnityEngine.PostProcessing.DitheringModel
struct DitheringModel_t2429005396;
// System.Func`2<UnityEngine.Vector2,UnityEngine.Matrix4x4>
struct Func_2_t4093140010;
// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>>
struct Dictionary_2_t1572824908;
// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase>
struct List_1_t4203178569;
// System.Collections.Generic.Dictionary`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean>
struct Dictionary_2_t3095696878;
// UnityEngine.PostProcessing.BuiltinDebugViewsComponent
struct BuiltinDebugViewsComponent_t2123147871;
// UnityEngine.PostProcessing.AmbientOcclusionComponent
struct AmbientOcclusionComponent_t4130625043;
// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent
struct ScreenSpaceReflectionComponent_t856094247;
// UnityEngine.PostProcessing.FogComponent
struct FogComponent_t3400726830;
// UnityEngine.PostProcessing.MotionBlurComponent
struct MotionBlurComponent_t3686516877;
// UnityEngine.PostProcessing.TaaComponent
struct TaaComponent_t3791749658;
// UnityEngine.PostProcessing.EyeAdaptationComponent
struct EyeAdaptationComponent_t3394805121;
// UnityEngine.PostProcessing.DepthOfFieldComponent
struct DepthOfFieldComponent_t554756766;
// UnityEngine.PostProcessing.BloomComponent
struct BloomComponent_t3791419130;
// UnityEngine.PostProcessing.ChromaticAberrationComponent
struct ChromaticAberrationComponent_t1647263118;
// UnityEngine.PostProcessing.ColorGradingComponent
struct ColorGradingComponent_t1715259467;
// UnityEngine.PostProcessing.UserLutComponent
struct UserLutComponent_t2843161776;
// UnityEngine.PostProcessing.GrainComponent
struct GrainComponent_t866324317;
// UnityEngine.PostProcessing.VignetteComponent
struct VignetteComponent_t3243642943;
// UnityEngine.PostProcessing.DitheringComponent
struct DitheringComponent_t277621267;
// UnityEngine.PostProcessing.FxaaComponent
struct FxaaComponent_t1312385771;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// UnityEngine.Rigidbody
struct Rigidbody_t3916780224;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef GRAPHICSUTILS_T2852986763_H
#define GRAPHICSUTILS_T2852986763_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GraphicsUtils
struct  GraphicsUtils_t2852986763  : public RuntimeObject
{
public:

public:
};

struct GraphicsUtils_t2852986763_StaticFields
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.GraphicsUtils::s_WhiteTexture
	Texture2D_t3840446185 * ___s_WhiteTexture_0;
	// UnityEngine.Mesh UnityEngine.PostProcessing.GraphicsUtils::s_Quad
	Mesh_t3648964284 * ___s_Quad_1;

public:
	inline static int32_t get_offset_of_s_WhiteTexture_0() { return static_cast<int32_t>(offsetof(GraphicsUtils_t2852986763_StaticFields, ___s_WhiteTexture_0)); }
	inline Texture2D_t3840446185 * get_s_WhiteTexture_0() const { return ___s_WhiteTexture_0; }
	inline Texture2D_t3840446185 ** get_address_of_s_WhiteTexture_0() { return &___s_WhiteTexture_0; }
	inline void set_s_WhiteTexture_0(Texture2D_t3840446185 * value)
	{
		___s_WhiteTexture_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_WhiteTexture_0), value);
	}

	inline static int32_t get_offset_of_s_Quad_1() { return static_cast<int32_t>(offsetof(GraphicsUtils_t2852986763_StaticFields, ___s_Quad_1)); }
	inline Mesh_t3648964284 * get_s_Quad_1() const { return ___s_Quad_1; }
	inline Mesh_t3648964284 ** get_address_of_s_Quad_1() { return &___s_Quad_1; }
	inline void set_s_Quad_1(Mesh_t3648964284 * value)
	{
		___s_Quad_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_Quad_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHICSUTILS_T2852986763_H
#ifndef MATERIALFACTORY_T2445948724_H
#define MATERIALFACTORY_T2445948724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MaterialFactory
struct  MaterialFactory_t2445948724  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> UnityEngine.PostProcessing.MaterialFactory::m_Materials
	Dictionary_2_t125631422 * ___m_Materials_0;

public:
	inline static int32_t get_offset_of_m_Materials_0() { return static_cast<int32_t>(offsetof(MaterialFactory_t2445948724, ___m_Materials_0)); }
	inline Dictionary_2_t125631422 * get_m_Materials_0() const { return ___m_Materials_0; }
	inline Dictionary_2_t125631422 ** get_address_of_m_Materials_0() { return &___m_Materials_0; }
	inline void set_m_Materials_0(Dictionary_2_t125631422 * value)
	{
		___m_Materials_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Materials_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALFACTORY_T2445948724_H
#ifndef UNIFORMS_T1233092826_H
#define UNIFORMS_T1233092826_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleWheelController/Uniforms
struct  Uniforms_t1233092826  : public RuntimeObject
{
public:

public:
};

struct Uniforms_t1233092826_StaticFields
{
public:
	// System.Int32 ExampleWheelController/Uniforms::_MotionAmount
	int32_t ____MotionAmount_0;

public:
	inline static int32_t get_offset_of__MotionAmount_0() { return static_cast<int32_t>(offsetof(Uniforms_t1233092826_StaticFields, ____MotionAmount_0)); }
	inline int32_t get__MotionAmount_0() const { return ____MotionAmount_0; }
	inline int32_t* get_address_of__MotionAmount_0() { return &____MotionAmount_0; }
	inline void set__MotionAmount_0(int32_t value)
	{
		____MotionAmount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIFORMS_T1233092826_H
#ifndef RENDERTEXTUREFACTORY_T1946967824_H
#define RENDERTEXTUREFACTORY_T1946967824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.RenderTextureFactory
struct  RenderTextureFactory_t1946967824  : public RuntimeObject
{
public:
	// System.Collections.Generic.HashSet`1<UnityEngine.RenderTexture> UnityEngine.PostProcessing.RenderTextureFactory::m_TemporaryRTs
	HashSet_1_t673836907 * ___m_TemporaryRTs_0;

public:
	inline static int32_t get_offset_of_m_TemporaryRTs_0() { return static_cast<int32_t>(offsetof(RenderTextureFactory_t1946967824, ___m_TemporaryRTs_0)); }
	inline HashSet_1_t673836907 * get_m_TemporaryRTs_0() const { return ___m_TemporaryRTs_0; }
	inline HashSet_1_t673836907 ** get_address_of_m_TemporaryRTs_0() { return &___m_TemporaryRTs_0; }
	inline void set_m_TemporaryRTs_0(HashSet_1_t673836907 * value)
	{
		___m_TemporaryRTs_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_TemporaryRTs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERTEXTUREFACTORY_T1946967824_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef COLORGRADINGCURVE_T2000571184_H
#define COLORGRADINGCURVE_T2000571184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingCurve
struct  ColorGradingCurve_t2000571184  : public RuntimeObject
{
public:
	// UnityEngine.AnimationCurve UnityEngine.PostProcessing.ColorGradingCurve::curve
	AnimationCurve_t3046754366 * ___curve_0;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingCurve::m_Loop
	bool ___m_Loop_1;
	// System.Single UnityEngine.PostProcessing.ColorGradingCurve::m_ZeroValue
	float ___m_ZeroValue_2;
	// System.Single UnityEngine.PostProcessing.ColorGradingCurve::m_Range
	float ___m_Range_3;
	// UnityEngine.AnimationCurve UnityEngine.PostProcessing.ColorGradingCurve::m_InternalLoopingCurve
	AnimationCurve_t3046754366 * ___m_InternalLoopingCurve_4;

public:
	inline static int32_t get_offset_of_curve_0() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t2000571184, ___curve_0)); }
	inline AnimationCurve_t3046754366 * get_curve_0() const { return ___curve_0; }
	inline AnimationCurve_t3046754366 ** get_address_of_curve_0() { return &___curve_0; }
	inline void set_curve_0(AnimationCurve_t3046754366 * value)
	{
		___curve_0 = value;
		Il2CppCodeGenWriteBarrier((&___curve_0), value);
	}

	inline static int32_t get_offset_of_m_Loop_1() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t2000571184, ___m_Loop_1)); }
	inline bool get_m_Loop_1() const { return ___m_Loop_1; }
	inline bool* get_address_of_m_Loop_1() { return &___m_Loop_1; }
	inline void set_m_Loop_1(bool value)
	{
		___m_Loop_1 = value;
	}

	inline static int32_t get_offset_of_m_ZeroValue_2() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t2000571184, ___m_ZeroValue_2)); }
	inline float get_m_ZeroValue_2() const { return ___m_ZeroValue_2; }
	inline float* get_address_of_m_ZeroValue_2() { return &___m_ZeroValue_2; }
	inline void set_m_ZeroValue_2(float value)
	{
		___m_ZeroValue_2 = value;
	}

	inline static int32_t get_offset_of_m_Range_3() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t2000571184, ___m_Range_3)); }
	inline float get_m_Range_3() const { return ___m_Range_3; }
	inline float* get_address_of_m_Range_3() { return &___m_Range_3; }
	inline void set_m_Range_3(float value)
	{
		___m_Range_3 = value;
	}

	inline static int32_t get_offset_of_m_InternalLoopingCurve_4() { return static_cast<int32_t>(offsetof(ColorGradingCurve_t2000571184, ___m_InternalLoopingCurve_4)); }
	inline AnimationCurve_t3046754366 * get_m_InternalLoopingCurve_4() const { return ___m_InternalLoopingCurve_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_m_InternalLoopingCurve_4() { return &___m_InternalLoopingCurve_4; }
	inline void set_m_InternalLoopingCurve_4(AnimationCurve_t3046754366 * value)
	{
		___m_InternalLoopingCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InternalLoopingCurve_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORGRADINGCURVE_T2000571184_H
#ifndef POSTPROCESSINGCONTEXT_T2014408948_H
#define POSTPROCESSINGCONTEXT_T2014408948_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingContext
struct  PostProcessingContext_t2014408948  : public RuntimeObject
{
public:
	// UnityEngine.PostProcessing.PostProcessingProfile UnityEngine.PostProcessing.PostProcessingContext::profile
	PostProcessingProfile_t724195375 * ___profile_0;
	// UnityEngine.Camera UnityEngine.PostProcessing.PostProcessingContext::camera
	Camera_t4157153871 * ___camera_1;
	// UnityEngine.PostProcessing.MaterialFactory UnityEngine.PostProcessing.PostProcessingContext::materialFactory
	MaterialFactory_t2445948724 * ___materialFactory_2;
	// UnityEngine.PostProcessing.RenderTextureFactory UnityEngine.PostProcessing.PostProcessingContext::renderTextureFactory
	RenderTextureFactory_t1946967824 * ___renderTextureFactory_3;
	// System.Boolean UnityEngine.PostProcessing.PostProcessingContext::<interrupted>k__BackingField
	bool ___U3CinterruptedU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_profile_0() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___profile_0)); }
	inline PostProcessingProfile_t724195375 * get_profile_0() const { return ___profile_0; }
	inline PostProcessingProfile_t724195375 ** get_address_of_profile_0() { return &___profile_0; }
	inline void set_profile_0(PostProcessingProfile_t724195375 * value)
	{
		___profile_0 = value;
		Il2CppCodeGenWriteBarrier((&___profile_0), value);
	}

	inline static int32_t get_offset_of_camera_1() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___camera_1)); }
	inline Camera_t4157153871 * get_camera_1() const { return ___camera_1; }
	inline Camera_t4157153871 ** get_address_of_camera_1() { return &___camera_1; }
	inline void set_camera_1(Camera_t4157153871 * value)
	{
		___camera_1 = value;
		Il2CppCodeGenWriteBarrier((&___camera_1), value);
	}

	inline static int32_t get_offset_of_materialFactory_2() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___materialFactory_2)); }
	inline MaterialFactory_t2445948724 * get_materialFactory_2() const { return ___materialFactory_2; }
	inline MaterialFactory_t2445948724 ** get_address_of_materialFactory_2() { return &___materialFactory_2; }
	inline void set_materialFactory_2(MaterialFactory_t2445948724 * value)
	{
		___materialFactory_2 = value;
		Il2CppCodeGenWriteBarrier((&___materialFactory_2), value);
	}

	inline static int32_t get_offset_of_renderTextureFactory_3() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___renderTextureFactory_3)); }
	inline RenderTextureFactory_t1946967824 * get_renderTextureFactory_3() const { return ___renderTextureFactory_3; }
	inline RenderTextureFactory_t1946967824 ** get_address_of_renderTextureFactory_3() { return &___renderTextureFactory_3; }
	inline void set_renderTextureFactory_3(RenderTextureFactory_t1946967824 * value)
	{
		___renderTextureFactory_3 = value;
		Il2CppCodeGenWriteBarrier((&___renderTextureFactory_3), value);
	}

	inline static int32_t get_offset_of_U3CinterruptedU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PostProcessingContext_t2014408948, ___U3CinterruptedU3Ek__BackingField_4)); }
	inline bool get_U3CinterruptedU3Ek__BackingField_4() const { return ___U3CinterruptedU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CinterruptedU3Ek__BackingField_4() { return &___U3CinterruptedU3Ek__BackingField_4; }
	inline void set_U3CinterruptedU3Ek__BackingField_4(bool value)
	{
		___U3CinterruptedU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCONTEXT_T2014408948_H
#ifndef POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#define POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingComponentBase
struct  PostProcessingComponentBase_t2731103827  : public RuntimeObject
{
public:
	// UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingComponentBase::context
	PostProcessingContext_t2014408948 * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(PostProcessingComponentBase_t2731103827, ___context_0)); }
	inline PostProcessingContext_t2014408948 * get_context_0() const { return ___context_0; }
	inline PostProcessingContext_t2014408948 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(PostProcessingContext_t2014408948 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGCOMPONENTBASE_T2731103827_H
#ifndef POSTPROCESSINGMODEL_T540111976_H
#define POSTPROCESSINGMODEL_T540111976_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingModel
struct  PostProcessingModel_t540111976  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.PostProcessing.PostProcessingModel::m_Enabled
	bool ___m_Enabled_0;

public:
	inline static int32_t get_offset_of_m_Enabled_0() { return static_cast<int32_t>(offsetof(PostProcessingModel_t540111976, ___m_Enabled_0)); }
	inline bool get_m_Enabled_0() const { return ___m_Enabled_0; }
	inline bool* get_address_of_m_Enabled_0() { return &___m_Enabled_0; }
	inline void set_m_Enabled_0(bool value)
	{
		___m_Enabled_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGMODEL_T540111976_H
#ifndef SETTINGS_T224798599_H
#define SETTINGS_T224798599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogModel/Settings
struct  Settings_t224798599 
{
public:
	// System.Boolean UnityEngine.PostProcessing.FogModel/Settings::excludeSkybox
	bool ___excludeSkybox_0;

public:
	inline static int32_t get_offset_of_excludeSkybox_0() { return static_cast<int32_t>(offsetof(Settings_t224798599, ___excludeSkybox_0)); }
	inline bool get_excludeSkybox_0() const { return ___excludeSkybox_0; }
	inline bool* get_address_of_excludeSkybox_0() { return &___excludeSkybox_0; }
	inline void set_excludeSkybox_0(bool value)
	{
		___excludeSkybox_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.FogModel/Settings
struct Settings_t224798599_marshaled_pinvoke
{
	int32_t ___excludeSkybox_0;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.FogModel/Settings
struct Settings_t224798599_marshaled_com
{
	int32_t ___excludeSkybox_0;
};
#endif // SETTINGS_T224798599_H
#ifndef INTENSITYSETTINGS_T1721872184_H
#define INTENSITYSETTINGS_T1721872184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings
struct  IntensitySettings_t1721872184 
{
public:
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings::reflectionMultiplier
	float ___reflectionMultiplier_0;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings::fadeDistance
	float ___fadeDistance_1;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings::fresnelFade
	float ___fresnelFade_2;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings::fresnelFadePower
	float ___fresnelFadePower_3;

public:
	inline static int32_t get_offset_of_reflectionMultiplier_0() { return static_cast<int32_t>(offsetof(IntensitySettings_t1721872184, ___reflectionMultiplier_0)); }
	inline float get_reflectionMultiplier_0() const { return ___reflectionMultiplier_0; }
	inline float* get_address_of_reflectionMultiplier_0() { return &___reflectionMultiplier_0; }
	inline void set_reflectionMultiplier_0(float value)
	{
		___reflectionMultiplier_0 = value;
	}

	inline static int32_t get_offset_of_fadeDistance_1() { return static_cast<int32_t>(offsetof(IntensitySettings_t1721872184, ___fadeDistance_1)); }
	inline float get_fadeDistance_1() const { return ___fadeDistance_1; }
	inline float* get_address_of_fadeDistance_1() { return &___fadeDistance_1; }
	inline void set_fadeDistance_1(float value)
	{
		___fadeDistance_1 = value;
	}

	inline static int32_t get_offset_of_fresnelFade_2() { return static_cast<int32_t>(offsetof(IntensitySettings_t1721872184, ___fresnelFade_2)); }
	inline float get_fresnelFade_2() const { return ___fresnelFade_2; }
	inline float* get_address_of_fresnelFade_2() { return &___fresnelFade_2; }
	inline void set_fresnelFade_2(float value)
	{
		___fresnelFade_2 = value;
	}

	inline static int32_t get_offset_of_fresnelFadePower_3() { return static_cast<int32_t>(offsetof(IntensitySettings_t1721872184, ___fresnelFadePower_3)); }
	inline float get_fresnelFadePower_3() const { return ___fresnelFadePower_3; }
	inline float* get_address_of_fresnelFadePower_3() { return &___fresnelFadePower_3; }
	inline void set_fresnelFadePower_3(float value)
	{
		___fresnelFadePower_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTENSITYSETTINGS_T1721872184_H
#ifndef SCREENEDGEMASK_T4063288584_H
#define SCREENEDGEMASK_T4063288584_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ScreenEdgeMask
struct  ScreenEdgeMask_t4063288584 
{
public:
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ScreenEdgeMask::intensity
	float ___intensity_0;

public:
	inline static int32_t get_offset_of_intensity_0() { return static_cast<int32_t>(offsetof(ScreenEdgeMask_t4063288584, ___intensity_0)); }
	inline float get_intensity_0() const { return ___intensity_0; }
	inline float* get_address_of_intensity_0() { return &___intensity_0; }
	inline void set_intensity_0(float value)
	{
		___intensity_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENEDGEMASK_T4063288584_H
#ifndef SETTINGS_T4282162361_H
#define SETTINGS_T4282162361_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurModel/Settings
struct  Settings_t4282162361 
{
public:
	// System.Single UnityEngine.PostProcessing.MotionBlurModel/Settings::shutterAngle
	float ___shutterAngle_0;
	// System.Int32 UnityEngine.PostProcessing.MotionBlurModel/Settings::sampleCount
	int32_t ___sampleCount_1;
	// System.Single UnityEngine.PostProcessing.MotionBlurModel/Settings::frameBlending
	float ___frameBlending_2;

public:
	inline static int32_t get_offset_of_shutterAngle_0() { return static_cast<int32_t>(offsetof(Settings_t4282162361, ___shutterAngle_0)); }
	inline float get_shutterAngle_0() const { return ___shutterAngle_0; }
	inline float* get_address_of_shutterAngle_0() { return &___shutterAngle_0; }
	inline void set_shutterAngle_0(float value)
	{
		___shutterAngle_0 = value;
	}

	inline static int32_t get_offset_of_sampleCount_1() { return static_cast<int32_t>(offsetof(Settings_t4282162361, ___sampleCount_1)); }
	inline int32_t get_sampleCount_1() const { return ___sampleCount_1; }
	inline int32_t* get_address_of_sampleCount_1() { return &___sampleCount_1; }
	inline void set_sampleCount_1(int32_t value)
	{
		___sampleCount_1 = value;
	}

	inline static int32_t get_offset_of_frameBlending_2() { return static_cast<int32_t>(offsetof(Settings_t4282162361, ___frameBlending_2)); }
	inline float get_frameBlending_2() const { return ___frameBlending_2; }
	inline float* get_address_of_frameBlending_2() { return &___frameBlending_2; }
	inline void set_frameBlending_2(float value)
	{
		___frameBlending_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T4282162361_H
#ifndef SETTINGS_T4123292438_H
#define SETTINGS_T4123292438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainModel/Settings
struct  Settings_t4123292438 
{
public:
	// System.Boolean UnityEngine.PostProcessing.GrainModel/Settings::colored
	bool ___colored_0;
	// System.Single UnityEngine.PostProcessing.GrainModel/Settings::intensity
	float ___intensity_1;
	// System.Single UnityEngine.PostProcessing.GrainModel/Settings::size
	float ___size_2;
	// System.Single UnityEngine.PostProcessing.GrainModel/Settings::luminanceContribution
	float ___luminanceContribution_3;

public:
	inline static int32_t get_offset_of_colored_0() { return static_cast<int32_t>(offsetof(Settings_t4123292438, ___colored_0)); }
	inline bool get_colored_0() const { return ___colored_0; }
	inline bool* get_address_of_colored_0() { return &___colored_0; }
	inline void set_colored_0(bool value)
	{
		___colored_0 = value;
	}

	inline static int32_t get_offset_of_intensity_1() { return static_cast<int32_t>(offsetof(Settings_t4123292438, ___intensity_1)); }
	inline float get_intensity_1() const { return ___intensity_1; }
	inline float* get_address_of_intensity_1() { return &___intensity_1; }
	inline void set_intensity_1(float value)
	{
		___intensity_1 = value;
	}

	inline static int32_t get_offset_of_size_2() { return static_cast<int32_t>(offsetof(Settings_t4123292438, ___size_2)); }
	inline float get_size_2() const { return ___size_2; }
	inline float* get_address_of_size_2() { return &___size_2; }
	inline void set_size_2(float value)
	{
		___size_2 = value;
	}

	inline static int32_t get_offset_of_luminanceContribution_3() { return static_cast<int32_t>(offsetof(Settings_t4123292438, ___luminanceContribution_3)); }
	inline float get_luminanceContribution_3() const { return ___luminanceContribution_3; }
	inline float* get_address_of_luminanceContribution_3() { return &___luminanceContribution_3; }
	inline void set_luminanceContribution_3(float value)
	{
		___luminanceContribution_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.GrainModel/Settings
struct Settings_t4123292438_marshaled_pinvoke
{
	int32_t ___colored_0;
	float ___intensity_1;
	float ___size_2;
	float ___luminanceContribution_3;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.GrainModel/Settings
struct Settings_t4123292438_marshaled_com
{
	int32_t ___colored_0;
	float ___intensity_1;
	float ___size_2;
	float ___luminanceContribution_3;
};
#endif // SETTINGS_T4123292438_H
#ifndef BASICSETTINGS_T838098426_H
#define BASICSETTINGS_T838098426_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/BasicSettings
struct  BasicSettings_t838098426 
{
public:
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::postExposure
	float ___postExposure_0;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::temperature
	float ___temperature_1;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::tint
	float ___tint_2;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::hueShift
	float ___hueShift_3;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::saturation
	float ___saturation_4;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/BasicSettings::contrast
	float ___contrast_5;

public:
	inline static int32_t get_offset_of_postExposure_0() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___postExposure_0)); }
	inline float get_postExposure_0() const { return ___postExposure_0; }
	inline float* get_address_of_postExposure_0() { return &___postExposure_0; }
	inline void set_postExposure_0(float value)
	{
		___postExposure_0 = value;
	}

	inline static int32_t get_offset_of_temperature_1() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___temperature_1)); }
	inline float get_temperature_1() const { return ___temperature_1; }
	inline float* get_address_of_temperature_1() { return &___temperature_1; }
	inline void set_temperature_1(float value)
	{
		___temperature_1 = value;
	}

	inline static int32_t get_offset_of_tint_2() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___tint_2)); }
	inline float get_tint_2() const { return ___tint_2; }
	inline float* get_address_of_tint_2() { return &___tint_2; }
	inline void set_tint_2(float value)
	{
		___tint_2 = value;
	}

	inline static int32_t get_offset_of_hueShift_3() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___hueShift_3)); }
	inline float get_hueShift_3() const { return ___hueShift_3; }
	inline float* get_address_of_hueShift_3() { return &___hueShift_3; }
	inline void set_hueShift_3(float value)
	{
		___hueShift_3 = value;
	}

	inline static int32_t get_offset_of_saturation_4() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___saturation_4)); }
	inline float get_saturation_4() const { return ___saturation_4; }
	inline float* get_address_of_saturation_4() { return &___saturation_4; }
	inline void set_saturation_4(float value)
	{
		___saturation_4 = value;
	}

	inline static int32_t get_offset_of_contrast_5() { return static_cast<int32_t>(offsetof(BasicSettings_t838098426, ___contrast_5)); }
	inline float get_contrast_5() const { return ___contrast_5; }
	inline float* get_address_of_contrast_5() { return &___contrast_5; }
	inline void set_contrast_5(float value)
	{
		___contrast_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICSETTINGS_T838098426_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef CURVESSETTINGS_T2830270037_H
#define CURVESSETTINGS_T2830270037_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings
struct  CurvesSettings_t2830270037 
{
public:
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::master
	ColorGradingCurve_t2000571184 * ___master_0;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::red
	ColorGradingCurve_t2000571184 * ___red_1;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::green
	ColorGradingCurve_t2000571184 * ___green_2;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::blue
	ColorGradingCurve_t2000571184 * ___blue_3;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::hueVShue
	ColorGradingCurve_t2000571184 * ___hueVShue_4;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::hueVSsat
	ColorGradingCurve_t2000571184 * ___hueVSsat_5;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::satVSsat
	ColorGradingCurve_t2000571184 * ___satVSsat_6;
	// UnityEngine.PostProcessing.ColorGradingCurve UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::lumVSsat
	ColorGradingCurve_t2000571184 * ___lumVSsat_7;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurrentEditingCurve
	int32_t ___e_CurrentEditingCurve_8;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveY
	bool ___e_CurveY_9;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveR
	bool ___e_CurveR_10;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveG
	bool ___e_CurveG_11;
	// System.Boolean UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings::e_CurveB
	bool ___e_CurveB_12;

public:
	inline static int32_t get_offset_of_master_0() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___master_0)); }
	inline ColorGradingCurve_t2000571184 * get_master_0() const { return ___master_0; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_master_0() { return &___master_0; }
	inline void set_master_0(ColorGradingCurve_t2000571184 * value)
	{
		___master_0 = value;
		Il2CppCodeGenWriteBarrier((&___master_0), value);
	}

	inline static int32_t get_offset_of_red_1() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___red_1)); }
	inline ColorGradingCurve_t2000571184 * get_red_1() const { return ___red_1; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_red_1() { return &___red_1; }
	inline void set_red_1(ColorGradingCurve_t2000571184 * value)
	{
		___red_1 = value;
		Il2CppCodeGenWriteBarrier((&___red_1), value);
	}

	inline static int32_t get_offset_of_green_2() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___green_2)); }
	inline ColorGradingCurve_t2000571184 * get_green_2() const { return ___green_2; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_green_2() { return &___green_2; }
	inline void set_green_2(ColorGradingCurve_t2000571184 * value)
	{
		___green_2 = value;
		Il2CppCodeGenWriteBarrier((&___green_2), value);
	}

	inline static int32_t get_offset_of_blue_3() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___blue_3)); }
	inline ColorGradingCurve_t2000571184 * get_blue_3() const { return ___blue_3; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_blue_3() { return &___blue_3; }
	inline void set_blue_3(ColorGradingCurve_t2000571184 * value)
	{
		___blue_3 = value;
		Il2CppCodeGenWriteBarrier((&___blue_3), value);
	}

	inline static int32_t get_offset_of_hueVShue_4() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___hueVShue_4)); }
	inline ColorGradingCurve_t2000571184 * get_hueVShue_4() const { return ___hueVShue_4; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_hueVShue_4() { return &___hueVShue_4; }
	inline void set_hueVShue_4(ColorGradingCurve_t2000571184 * value)
	{
		___hueVShue_4 = value;
		Il2CppCodeGenWriteBarrier((&___hueVShue_4), value);
	}

	inline static int32_t get_offset_of_hueVSsat_5() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___hueVSsat_5)); }
	inline ColorGradingCurve_t2000571184 * get_hueVSsat_5() const { return ___hueVSsat_5; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_hueVSsat_5() { return &___hueVSsat_5; }
	inline void set_hueVSsat_5(ColorGradingCurve_t2000571184 * value)
	{
		___hueVSsat_5 = value;
		Il2CppCodeGenWriteBarrier((&___hueVSsat_5), value);
	}

	inline static int32_t get_offset_of_satVSsat_6() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___satVSsat_6)); }
	inline ColorGradingCurve_t2000571184 * get_satVSsat_6() const { return ___satVSsat_6; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_satVSsat_6() { return &___satVSsat_6; }
	inline void set_satVSsat_6(ColorGradingCurve_t2000571184 * value)
	{
		___satVSsat_6 = value;
		Il2CppCodeGenWriteBarrier((&___satVSsat_6), value);
	}

	inline static int32_t get_offset_of_lumVSsat_7() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___lumVSsat_7)); }
	inline ColorGradingCurve_t2000571184 * get_lumVSsat_7() const { return ___lumVSsat_7; }
	inline ColorGradingCurve_t2000571184 ** get_address_of_lumVSsat_7() { return &___lumVSsat_7; }
	inline void set_lumVSsat_7(ColorGradingCurve_t2000571184 * value)
	{
		___lumVSsat_7 = value;
		Il2CppCodeGenWriteBarrier((&___lumVSsat_7), value);
	}

	inline static int32_t get_offset_of_e_CurrentEditingCurve_8() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurrentEditingCurve_8)); }
	inline int32_t get_e_CurrentEditingCurve_8() const { return ___e_CurrentEditingCurve_8; }
	inline int32_t* get_address_of_e_CurrentEditingCurve_8() { return &___e_CurrentEditingCurve_8; }
	inline void set_e_CurrentEditingCurve_8(int32_t value)
	{
		___e_CurrentEditingCurve_8 = value;
	}

	inline static int32_t get_offset_of_e_CurveY_9() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveY_9)); }
	inline bool get_e_CurveY_9() const { return ___e_CurveY_9; }
	inline bool* get_address_of_e_CurveY_9() { return &___e_CurveY_9; }
	inline void set_e_CurveY_9(bool value)
	{
		___e_CurveY_9 = value;
	}

	inline static int32_t get_offset_of_e_CurveR_10() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveR_10)); }
	inline bool get_e_CurveR_10() const { return ___e_CurveR_10; }
	inline bool* get_address_of_e_CurveR_10() { return &___e_CurveR_10; }
	inline void set_e_CurveR_10(bool value)
	{
		___e_CurveR_10 = value;
	}

	inline static int32_t get_offset_of_e_CurveG_11() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveG_11)); }
	inline bool get_e_CurveG_11() const { return ___e_CurveG_11; }
	inline bool* get_address_of_e_CurveG_11() { return &___e_CurveG_11; }
	inline void set_e_CurveG_11(bool value)
	{
		___e_CurveG_11 = value;
	}

	inline static int32_t get_offset_of_e_CurveB_12() { return static_cast<int32_t>(offsetof(CurvesSettings_t2830270037, ___e_CurveB_12)); }
	inline bool get_e_CurveB_12() const { return ___e_CurveB_12; }
	inline bool* get_address_of_e_CurveB_12() { return &___e_CurveB_12; }
	inline void set_e_CurveB_12(bool value)
	{
		___e_CurveB_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings
struct CurvesSettings_t2830270037_marshaled_pinvoke
{
	ColorGradingCurve_t2000571184 * ___master_0;
	ColorGradingCurve_t2000571184 * ___red_1;
	ColorGradingCurve_t2000571184 * ___green_2;
	ColorGradingCurve_t2000571184 * ___blue_3;
	ColorGradingCurve_t2000571184 * ___hueVShue_4;
	ColorGradingCurve_t2000571184 * ___hueVSsat_5;
	ColorGradingCurve_t2000571184 * ___satVSsat_6;
	ColorGradingCurve_t2000571184 * ___lumVSsat_7;
	int32_t ___e_CurrentEditingCurve_8;
	int32_t ___e_CurveY_9;
	int32_t ___e_CurveR_10;
	int32_t ___e_CurveG_11;
	int32_t ___e_CurveB_12;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings
struct CurvesSettings_t2830270037_marshaled_com
{
	ColorGradingCurve_t2000571184 * ___master_0;
	ColorGradingCurve_t2000571184 * ___red_1;
	ColorGradingCurve_t2000571184 * ___green_2;
	ColorGradingCurve_t2000571184 * ___blue_3;
	ColorGradingCurve_t2000571184 * ___hueVShue_4;
	ColorGradingCurve_t2000571184 * ___hueVSsat_5;
	ColorGradingCurve_t2000571184 * ___satVSsat_6;
	ColorGradingCurve_t2000571184 * ___lumVSsat_7;
	int32_t ___e_CurrentEditingCurve_8;
	int32_t ___e_CurveY_9;
	int32_t ___e_CurveR_10;
	int32_t ___e_CurveG_11;
	int32_t ___e_CurveB_12;
};
#endif // CURVESSETTINGS_T2830270037_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef U24ARRAYTYPEU3D12_T2488454197_H
#define U24ARRAYTYPEU3D12_T2488454197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=12
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D12_t2488454197 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D12_t2488454197__padding[12];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D12_T2488454197_H
#ifndef SETTINGS_T3006579223_H
#define SETTINGS_T3006579223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.UserLutModel/Settings
struct  Settings_t3006579223 
{
public:
	// UnityEngine.Texture2D UnityEngine.PostProcessing.UserLutModel/Settings::lut
	Texture2D_t3840446185 * ___lut_0;
	// System.Single UnityEngine.PostProcessing.UserLutModel/Settings::contribution
	float ___contribution_1;

public:
	inline static int32_t get_offset_of_lut_0() { return static_cast<int32_t>(offsetof(Settings_t3006579223, ___lut_0)); }
	inline Texture2D_t3840446185 * get_lut_0() const { return ___lut_0; }
	inline Texture2D_t3840446185 ** get_address_of_lut_0() { return &___lut_0; }
	inline void set_lut_0(Texture2D_t3840446185 * value)
	{
		___lut_0 = value;
		Il2CppCodeGenWriteBarrier((&___lut_0), value);
	}

	inline static int32_t get_offset_of_contribution_1() { return static_cast<int32_t>(offsetof(Settings_t3006579223, ___contribution_1)); }
	inline float get_contribution_1() const { return ___contribution_1; }
	inline float* get_address_of_contribution_1() { return &___contribution_1; }
	inline void set_contribution_1(float value)
	{
		___contribution_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.UserLutModel/Settings
struct Settings_t3006579223_marshaled_pinvoke
{
	Texture2D_t3840446185 * ___lut_0;
	float ___contribution_1;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.UserLutModel/Settings
struct Settings_t3006579223_marshaled_com
{
	Texture2D_t3840446185 * ___lut_0;
	float ___contribution_1;
};
#endif // SETTINGS_T3006579223_H
#ifndef U24ARRAYTYPEU3D24_T2467506693_H
#define U24ARRAYTYPEU3D24_T2467506693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/$ArrayType=24
#pragma pack(push, tp, 1)
struct  U24ArrayTypeU3D24_t2467506693 
{
public:
	union
	{
		struct
		{
		};
		uint8_t U24ArrayTypeU3D24_t2467506693__padding[24];
	};

public:
};
#pragma pack(pop, tp)

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U24ARRAYTYPEU3D24_T2467506693_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef SETTINGS_T2313396630_H
#define SETTINGS_T2313396630_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringModel/Settings
struct  Settings_t2313396630 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Settings_t2313396630__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGS_T2313396630_H
#ifndef U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#define U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>
struct  U3CPrivateImplementationDetailsU3E_t3057255366  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields
{
public:
	// <PrivateImplementationDetails>/$ArrayType=12 <PrivateImplementationDetails>::$field-51A7A390CD6DE245186881400B18C9D822EFE240
	U24ArrayTypeU3D12_t2488454197  ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_0;
	// <PrivateImplementationDetails>/$ArrayType=24 <PrivateImplementationDetails>::$field-C90F38A020811481753795774EB5AF353F414C59
	U24ArrayTypeU3D24_t2467506693  ___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_1;

public:
	inline static int32_t get_offset_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_0)); }
	inline U24ArrayTypeU3D12_t2488454197  get_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_0() const { return ___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_0; }
	inline U24ArrayTypeU3D12_t2488454197 * get_address_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_0() { return &___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_0; }
	inline void set_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_0(U24ArrayTypeU3D12_t2488454197  value)
	{
		___U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_0 = value;
	}

	inline static int32_t get_offset_of_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields, ___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_1)); }
	inline U24ArrayTypeU3D24_t2467506693  get_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_1() const { return ___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_1; }
	inline U24ArrayTypeU3D24_t2467506693 * get_address_of_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_1() { return &___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_1; }
	inline void set_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_1(U24ArrayTypeU3D24_t2467506693  value)
	{
		___U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPRIVATEIMPLEMENTATIONDETAILSU3E_T3057255366_H
#ifndef MODE_T3936508933_H
#define MODE_T3936508933_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteModel/Mode
struct  Mode_t3936508933 
{
public:
	// System.Int32 UnityEngine.PostProcessing.VignetteModel/Mode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Mode_t3936508933, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T3936508933_H
#ifndef TONEMAPPER_T1404353651_H
#define TONEMAPPER_T1404353651_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/Tonemapper
struct  Tonemapper_t1404353651 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/Tonemapper::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Tonemapper_t1404353651, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPER_T1404353651_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef CHANNELMIXERSETTINGS_T898701698_H
#define CHANNELMIXERSETTINGS_T898701698_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings
struct  ChannelMixerSettings_t898701698 
{
public:
	// UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::red
	Vector3_t3722313464  ___red_0;
	// UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::green
	Vector3_t3722313464  ___green_1;
	// UnityEngine.Vector3 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::blue
	Vector3_t3722313464  ___blue_2;
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings::currentEditingChannel
	int32_t ___currentEditingChannel_3;

public:
	inline static int32_t get_offset_of_red_0() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___red_0)); }
	inline Vector3_t3722313464  get_red_0() const { return ___red_0; }
	inline Vector3_t3722313464 * get_address_of_red_0() { return &___red_0; }
	inline void set_red_0(Vector3_t3722313464  value)
	{
		___red_0 = value;
	}

	inline static int32_t get_offset_of_green_1() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___green_1)); }
	inline Vector3_t3722313464  get_green_1() const { return ___green_1; }
	inline Vector3_t3722313464 * get_address_of_green_1() { return &___green_1; }
	inline void set_green_1(Vector3_t3722313464  value)
	{
		___green_1 = value;
	}

	inline static int32_t get_offset_of_blue_2() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___blue_2)); }
	inline Vector3_t3722313464  get_blue_2() const { return ___blue_2; }
	inline Vector3_t3722313464 * get_address_of_blue_2() { return &___blue_2; }
	inline void set_blue_2(Vector3_t3722313464  value)
	{
		___blue_2 = value;
	}

	inline static int32_t get_offset_of_currentEditingChannel_3() { return static_cast<int32_t>(offsetof(ChannelMixerSettings_t898701698, ___currentEditingChannel_3)); }
	inline int32_t get_currentEditingChannel_3() const { return ___currentEditingChannel_3; }
	inline int32_t* get_address_of_currentEditingChannel_3() { return &___currentEditingChannel_3; }
	inline void set_currentEditingChannel_3(int32_t value)
	{
		___currentEditingChannel_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHANNELMIXERSETTINGS_T898701698_H
#ifndef LINEARWHEELSSETTINGS_T3897781309_H
#define LINEARWHEELSSETTINGS_T3897781309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings
struct  LinearWheelsSettings_t3897781309 
{
public:
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::lift
	Color_t2555686324  ___lift_0;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::gamma
	Color_t2555686324  ___gamma_1;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings::gain
	Color_t2555686324  ___gain_2;

public:
	inline static int32_t get_offset_of_lift_0() { return static_cast<int32_t>(offsetof(LinearWheelsSettings_t3897781309, ___lift_0)); }
	inline Color_t2555686324  get_lift_0() const { return ___lift_0; }
	inline Color_t2555686324 * get_address_of_lift_0() { return &___lift_0; }
	inline void set_lift_0(Color_t2555686324  value)
	{
		___lift_0 = value;
	}

	inline static int32_t get_offset_of_gamma_1() { return static_cast<int32_t>(offsetof(LinearWheelsSettings_t3897781309, ___gamma_1)); }
	inline Color_t2555686324  get_gamma_1() const { return ___gamma_1; }
	inline Color_t2555686324 * get_address_of_gamma_1() { return &___gamma_1; }
	inline void set_gamma_1(Color_t2555686324  value)
	{
		___gamma_1 = value;
	}

	inline static int32_t get_offset_of_gain_2() { return static_cast<int32_t>(offsetof(LinearWheelsSettings_t3897781309, ___gain_2)); }
	inline Color_t2555686324  get_gain_2() const { return ___gain_2; }
	inline Color_t2555686324 * get_address_of_gain_2() { return &___gain_2; }
	inline void set_gain_2(Color_t2555686324  value)
	{
		___gain_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEARWHEELSSETTINGS_T3897781309_H
#ifndef FOGMODEL_T3620688749_H
#define FOGMODEL_T3620688749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.FogModel
struct  FogModel_t3620688749  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.FogModel/Settings UnityEngine.PostProcessing.FogModel::m_Settings
	Settings_t224798599  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(FogModel_t3620688749, ___m_Settings_1)); }
	inline Settings_t224798599  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t224798599 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t224798599  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOGMODEL_T3620688749_H
#ifndef MOTIONBLURMODEL_T3080286123_H
#define MOTIONBLURMODEL_T3080286123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.MotionBlurModel
struct  MotionBlurModel_t3080286123  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.MotionBlurModel/Settings UnityEngine.PostProcessing.MotionBlurModel::m_Settings
	Settings_t4282162361  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(MotionBlurModel_t3080286123, ___m_Settings_1)); }
	inline Settings_t4282162361  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t4282162361 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t4282162361  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONBLURMODEL_T3080286123_H
#ifndef GRAINMODEL_T1152882488_H
#define GRAINMODEL_T1152882488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.GrainModel
struct  GrainModel_t1152882488  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.GrainModel/Settings UnityEngine.PostProcessing.GrainModel::m_Settings
	Settings_t4123292438  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(GrainModel_t1152882488, ___m_Settings_1)); }
	inline Settings_t4123292438  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t4123292438 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t4123292438  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAINMODEL_T1152882488_H
#ifndef KERNELSIZE_T2406218613_H
#define KERNELSIZE_T2406218613_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldModel/KernelSize
struct  KernelSize_t2406218613 
{
public:
	// System.Int32 UnityEngine.PostProcessing.DepthOfFieldModel/KernelSize::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(KernelSize_t2406218613, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KERNELSIZE_T2406218613_H
#ifndef COLORWHEELMODE_T1939415375_H
#define COLORWHEELMODE_T1939415375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelMode
struct  ColorWheelMode_t1939415375 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ColorGradingModel/ColorWheelMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ColorWheelMode_t1939415375, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWHEELMODE_T1939415375_H
#ifndef EYEADAPTATIONTYPE_T3053468307_H
#define EYEADAPTATIONTYPE_T3053468307_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationModel/EyeAdaptationType
struct  EyeAdaptationType_t3053468307 
{
public:
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationModel/EyeAdaptationType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(EyeAdaptationType_t3053468307, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATIONTYPE_T3053468307_H
#ifndef DITHERINGMODEL_T2429005396_H
#define DITHERINGMODEL_T2429005396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DitheringModel
struct  DitheringModel_t2429005396  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.DitheringModel/Settings UnityEngine.PostProcessing.DitheringModel::m_Settings
	Settings_t2313396630  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(DitheringModel_t2429005396, ___m_Settings_1)); }
	inline Settings_t2313396630  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t2313396630 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t2313396630  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DITHERINGMODEL_T2429005396_H
#ifndef LOGWHEELSSETTINGS_T1545220311_H
#define LOGWHEELSSETTINGS_T1545220311_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings
struct  LogWheelsSettings_t1545220311 
{
public:
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::slope
	Color_t2555686324  ___slope_0;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::power
	Color_t2555686324  ___power_1;
	// UnityEngine.Color UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings::offset
	Color_t2555686324  ___offset_2;

public:
	inline static int32_t get_offset_of_slope_0() { return static_cast<int32_t>(offsetof(LogWheelsSettings_t1545220311, ___slope_0)); }
	inline Color_t2555686324  get_slope_0() const { return ___slope_0; }
	inline Color_t2555686324 * get_address_of_slope_0() { return &___slope_0; }
	inline void set_slope_0(Color_t2555686324  value)
	{
		___slope_0 = value;
	}

	inline static int32_t get_offset_of_power_1() { return static_cast<int32_t>(offsetof(LogWheelsSettings_t1545220311, ___power_1)); }
	inline Color_t2555686324  get_power_1() const { return ___power_1; }
	inline Color_t2555686324 * get_address_of_power_1() { return &___power_1; }
	inline void set_power_1(Color_t2555686324  value)
	{
		___power_1 = value;
	}

	inline static int32_t get_offset_of_offset_2() { return static_cast<int32_t>(offsetof(LogWheelsSettings_t1545220311, ___offset_2)); }
	inline Color_t2555686324  get_offset_2() const { return ___offset_2; }
	inline Color_t2555686324 * get_address_of_offset_2() { return &___offset_2; }
	inline void set_offset_2(Color_t2555686324  value)
	{
		___offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGWHEELSSETTINGS_T1545220311_H
#ifndef USERLUTMODEL_T1670108080_H
#define USERLUTMODEL_T1670108080_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.UserLutModel
struct  UserLutModel_t1670108080  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.UserLutModel/Settings UnityEngine.PostProcessing.UserLutModel::m_Settings
	Settings_t3006579223  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(UserLutModel_t1670108080, ___m_Settings_1)); }
	inline Settings_t3006579223  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t3006579223 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t3006579223  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USERLUTMODEL_T1670108080_H
#ifndef SSRREFLECTIONBLENDTYPE_T3026770880_H
#define SSRREFLECTIONBLENDTYPE_T3026770880_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRReflectionBlendType
struct  SSRReflectionBlendType_t3026770880 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRReflectionBlendType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SSRReflectionBlendType_t3026770880, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSRREFLECTIONBLENDTYPE_T3026770880_H
#ifndef SSRRESOLUTION_T161222554_H
#define SSRRESOLUTION_T161222554_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRResolution
struct  SSRResolution_t161222554 
{
public:
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRResolution::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(SSRResolution_t161222554, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SSRRESOLUTION_T161222554_H
#ifndef SETTINGS_T2195468135_H
#define SETTINGS_T2195468135_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldModel/Settings
struct  Settings_t2195468135 
{
public:
	// System.Single UnityEngine.PostProcessing.DepthOfFieldModel/Settings::focusDistance
	float ___focusDistance_0;
	// System.Single UnityEngine.PostProcessing.DepthOfFieldModel/Settings::aperture
	float ___aperture_1;
	// System.Single UnityEngine.PostProcessing.DepthOfFieldModel/Settings::focalLength
	float ___focalLength_2;
	// System.Boolean UnityEngine.PostProcessing.DepthOfFieldModel/Settings::useCameraFov
	bool ___useCameraFov_3;
	// UnityEngine.PostProcessing.DepthOfFieldModel/KernelSize UnityEngine.PostProcessing.DepthOfFieldModel/Settings::kernelSize
	int32_t ___kernelSize_4;

public:
	inline static int32_t get_offset_of_focusDistance_0() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___focusDistance_0)); }
	inline float get_focusDistance_0() const { return ___focusDistance_0; }
	inline float* get_address_of_focusDistance_0() { return &___focusDistance_0; }
	inline void set_focusDistance_0(float value)
	{
		___focusDistance_0 = value;
	}

	inline static int32_t get_offset_of_aperture_1() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___aperture_1)); }
	inline float get_aperture_1() const { return ___aperture_1; }
	inline float* get_address_of_aperture_1() { return &___aperture_1; }
	inline void set_aperture_1(float value)
	{
		___aperture_1 = value;
	}

	inline static int32_t get_offset_of_focalLength_2() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___focalLength_2)); }
	inline float get_focalLength_2() const { return ___focalLength_2; }
	inline float* get_address_of_focalLength_2() { return &___focalLength_2; }
	inline void set_focalLength_2(float value)
	{
		___focalLength_2 = value;
	}

	inline static int32_t get_offset_of_useCameraFov_3() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___useCameraFov_3)); }
	inline bool get_useCameraFov_3() const { return ___useCameraFov_3; }
	inline bool* get_address_of_useCameraFov_3() { return &___useCameraFov_3; }
	inline void set_useCameraFov_3(bool value)
	{
		___useCameraFov_3 = value;
	}

	inline static int32_t get_offset_of_kernelSize_4() { return static_cast<int32_t>(offsetof(Settings_t2195468135, ___kernelSize_4)); }
	inline int32_t get_kernelSize_4() const { return ___kernelSize_4; }
	inline int32_t* get_address_of_kernelSize_4() { return &___kernelSize_4; }
	inline void set_kernelSize_4(int32_t value)
	{
		___kernelSize_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.DepthOfFieldModel/Settings
struct Settings_t2195468135_marshaled_pinvoke
{
	float ___focusDistance_0;
	float ___aperture_1;
	float ___focalLength_2;
	int32_t ___useCameraFov_3;
	int32_t ___kernelSize_4;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.DepthOfFieldModel/Settings
struct Settings_t2195468135_marshaled_com
{
	float ___focusDistance_0;
	float ___aperture_1;
	float ___focalLength_2;
	int32_t ___useCameraFov_3;
	int32_t ___kernelSize_4;
};
#endif // SETTINGS_T2195468135_H
#ifndef SETTINGS_T1354494600_H
#define SETTINGS_T1354494600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteModel/Settings
struct  Settings_t1354494600 
{
public:
	// UnityEngine.PostProcessing.VignetteModel/Mode UnityEngine.PostProcessing.VignetteModel/Settings::mode
	int32_t ___mode_0;
	// UnityEngine.Color UnityEngine.PostProcessing.VignetteModel/Settings::color
	Color_t2555686324  ___color_1;
	// UnityEngine.Vector2 UnityEngine.PostProcessing.VignetteModel/Settings::center
	Vector2_t2156229523  ___center_2;
	// System.Single UnityEngine.PostProcessing.VignetteModel/Settings::intensity
	float ___intensity_3;
	// System.Single UnityEngine.PostProcessing.VignetteModel/Settings::smoothness
	float ___smoothness_4;
	// System.Single UnityEngine.PostProcessing.VignetteModel/Settings::roundness
	float ___roundness_5;
	// UnityEngine.Texture UnityEngine.PostProcessing.VignetteModel/Settings::mask
	Texture_t3661962703 * ___mask_6;
	// System.Single UnityEngine.PostProcessing.VignetteModel/Settings::opacity
	float ___opacity_7;
	// System.Boolean UnityEngine.PostProcessing.VignetteModel/Settings::rounded
	bool ___rounded_8;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_color_1() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___color_1)); }
	inline Color_t2555686324  get_color_1() const { return ___color_1; }
	inline Color_t2555686324 * get_address_of_color_1() { return &___color_1; }
	inline void set_color_1(Color_t2555686324  value)
	{
		___color_1 = value;
	}

	inline static int32_t get_offset_of_center_2() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___center_2)); }
	inline Vector2_t2156229523  get_center_2() const { return ___center_2; }
	inline Vector2_t2156229523 * get_address_of_center_2() { return &___center_2; }
	inline void set_center_2(Vector2_t2156229523  value)
	{
		___center_2 = value;
	}

	inline static int32_t get_offset_of_intensity_3() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___intensity_3)); }
	inline float get_intensity_3() const { return ___intensity_3; }
	inline float* get_address_of_intensity_3() { return &___intensity_3; }
	inline void set_intensity_3(float value)
	{
		___intensity_3 = value;
	}

	inline static int32_t get_offset_of_smoothness_4() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___smoothness_4)); }
	inline float get_smoothness_4() const { return ___smoothness_4; }
	inline float* get_address_of_smoothness_4() { return &___smoothness_4; }
	inline void set_smoothness_4(float value)
	{
		___smoothness_4 = value;
	}

	inline static int32_t get_offset_of_roundness_5() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___roundness_5)); }
	inline float get_roundness_5() const { return ___roundness_5; }
	inline float* get_address_of_roundness_5() { return &___roundness_5; }
	inline void set_roundness_5(float value)
	{
		___roundness_5 = value;
	}

	inline static int32_t get_offset_of_mask_6() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___mask_6)); }
	inline Texture_t3661962703 * get_mask_6() const { return ___mask_6; }
	inline Texture_t3661962703 ** get_address_of_mask_6() { return &___mask_6; }
	inline void set_mask_6(Texture_t3661962703 * value)
	{
		___mask_6 = value;
		Il2CppCodeGenWriteBarrier((&___mask_6), value);
	}

	inline static int32_t get_offset_of_opacity_7() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___opacity_7)); }
	inline float get_opacity_7() const { return ___opacity_7; }
	inline float* get_address_of_opacity_7() { return &___opacity_7; }
	inline void set_opacity_7(float value)
	{
		___opacity_7 = value;
	}

	inline static int32_t get_offset_of_rounded_8() { return static_cast<int32_t>(offsetof(Settings_t1354494600, ___rounded_8)); }
	inline bool get_rounded_8() const { return ___rounded_8; }
	inline bool* get_address_of_rounded_8() { return &___rounded_8; }
	inline void set_rounded_8(bool value)
	{
		___rounded_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.VignetteModel/Settings
struct Settings_t1354494600_marshaled_pinvoke
{
	int32_t ___mode_0;
	Color_t2555686324  ___color_1;
	Vector2_t2156229523  ___center_2;
	float ___intensity_3;
	float ___smoothness_4;
	float ___roundness_5;
	Texture_t3661962703 * ___mask_6;
	float ___opacity_7;
	int32_t ___rounded_8;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.VignetteModel/Settings
struct Settings_t1354494600_marshaled_com
{
	int32_t ___mode_0;
	Color_t2555686324  ___color_1;
	Vector2_t2156229523  ___center_2;
	float ___intensity_3;
	float ___smoothness_4;
	float ___roundness_5;
	Texture_t3661962703 * ___mask_6;
	float ___opacity_7;
	int32_t ___rounded_8;
};
#endif // SETTINGS_T1354494600_H
#ifndef TONEMAPPINGSETTINGS_T4154044775_H
#define TONEMAPPINGSETTINGS_T4154044775_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings
struct  TonemappingSettings_t4154044775 
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/Tonemapper UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::tonemapper
	int32_t ___tonemapper_0;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralBlackIn
	float ___neutralBlackIn_1;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteIn
	float ___neutralWhiteIn_2;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralBlackOut
	float ___neutralBlackOut_3;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteOut
	float ___neutralWhiteOut_4;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteLevel
	float ___neutralWhiteLevel_5;
	// System.Single UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings::neutralWhiteClip
	float ___neutralWhiteClip_6;

public:
	inline static int32_t get_offset_of_tonemapper_0() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___tonemapper_0)); }
	inline int32_t get_tonemapper_0() const { return ___tonemapper_0; }
	inline int32_t* get_address_of_tonemapper_0() { return &___tonemapper_0; }
	inline void set_tonemapper_0(int32_t value)
	{
		___tonemapper_0 = value;
	}

	inline static int32_t get_offset_of_neutralBlackIn_1() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralBlackIn_1)); }
	inline float get_neutralBlackIn_1() const { return ___neutralBlackIn_1; }
	inline float* get_address_of_neutralBlackIn_1() { return &___neutralBlackIn_1; }
	inline void set_neutralBlackIn_1(float value)
	{
		___neutralBlackIn_1 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteIn_2() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteIn_2)); }
	inline float get_neutralWhiteIn_2() const { return ___neutralWhiteIn_2; }
	inline float* get_address_of_neutralWhiteIn_2() { return &___neutralWhiteIn_2; }
	inline void set_neutralWhiteIn_2(float value)
	{
		___neutralWhiteIn_2 = value;
	}

	inline static int32_t get_offset_of_neutralBlackOut_3() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralBlackOut_3)); }
	inline float get_neutralBlackOut_3() const { return ___neutralBlackOut_3; }
	inline float* get_address_of_neutralBlackOut_3() { return &___neutralBlackOut_3; }
	inline void set_neutralBlackOut_3(float value)
	{
		___neutralBlackOut_3 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteOut_4() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteOut_4)); }
	inline float get_neutralWhiteOut_4() const { return ___neutralWhiteOut_4; }
	inline float* get_address_of_neutralWhiteOut_4() { return &___neutralWhiteOut_4; }
	inline void set_neutralWhiteOut_4(float value)
	{
		___neutralWhiteOut_4 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteLevel_5() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteLevel_5)); }
	inline float get_neutralWhiteLevel_5() const { return ___neutralWhiteLevel_5; }
	inline float* get_address_of_neutralWhiteLevel_5() { return &___neutralWhiteLevel_5; }
	inline void set_neutralWhiteLevel_5(float value)
	{
		___neutralWhiteLevel_5 = value;
	}

	inline static int32_t get_offset_of_neutralWhiteClip_6() { return static_cast<int32_t>(offsetof(TonemappingSettings_t4154044775, ___neutralWhiteClip_6)); }
	inline float get_neutralWhiteClip_6() const { return ___neutralWhiteClip_6; }
	inline float* get_address_of_neutralWhiteClip_6() { return &___neutralWhiteClip_6; }
	inline void set_neutralWhiteClip_6(float value)
	{
		___neutralWhiteClip_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TONEMAPPINGSETTINGS_T4154044775_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef REFLECTIONSETTINGS_T282755190_H
#define REFLECTIONSETTINGS_T282755190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
struct  ReflectionSettings_t282755190 
{
public:
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRReflectionBlendType UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::blendType
	int32_t ___blendType_0;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/SSRResolution UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::reflectionQuality
	int32_t ___reflectionQuality_1;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::maxDistance
	float ___maxDistance_2;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::iterationCount
	int32_t ___iterationCount_3;
	// System.Int32 UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::stepSize
	int32_t ___stepSize_4;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::widthModifier
	float ___widthModifier_5;
	// System.Single UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::reflectionBlur
	float ___reflectionBlur_6;
	// System.Boolean UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings::reflectBackfaces
	bool ___reflectBackfaces_7;

public:
	inline static int32_t get_offset_of_blendType_0() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___blendType_0)); }
	inline int32_t get_blendType_0() const { return ___blendType_0; }
	inline int32_t* get_address_of_blendType_0() { return &___blendType_0; }
	inline void set_blendType_0(int32_t value)
	{
		___blendType_0 = value;
	}

	inline static int32_t get_offset_of_reflectionQuality_1() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___reflectionQuality_1)); }
	inline int32_t get_reflectionQuality_1() const { return ___reflectionQuality_1; }
	inline int32_t* get_address_of_reflectionQuality_1() { return &___reflectionQuality_1; }
	inline void set_reflectionQuality_1(int32_t value)
	{
		___reflectionQuality_1 = value;
	}

	inline static int32_t get_offset_of_maxDistance_2() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___maxDistance_2)); }
	inline float get_maxDistance_2() const { return ___maxDistance_2; }
	inline float* get_address_of_maxDistance_2() { return &___maxDistance_2; }
	inline void set_maxDistance_2(float value)
	{
		___maxDistance_2 = value;
	}

	inline static int32_t get_offset_of_iterationCount_3() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___iterationCount_3)); }
	inline int32_t get_iterationCount_3() const { return ___iterationCount_3; }
	inline int32_t* get_address_of_iterationCount_3() { return &___iterationCount_3; }
	inline void set_iterationCount_3(int32_t value)
	{
		___iterationCount_3 = value;
	}

	inline static int32_t get_offset_of_stepSize_4() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___stepSize_4)); }
	inline int32_t get_stepSize_4() const { return ___stepSize_4; }
	inline int32_t* get_address_of_stepSize_4() { return &___stepSize_4; }
	inline void set_stepSize_4(int32_t value)
	{
		___stepSize_4 = value;
	}

	inline static int32_t get_offset_of_widthModifier_5() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___widthModifier_5)); }
	inline float get_widthModifier_5() const { return ___widthModifier_5; }
	inline float* get_address_of_widthModifier_5() { return &___widthModifier_5; }
	inline void set_widthModifier_5(float value)
	{
		___widthModifier_5 = value;
	}

	inline static int32_t get_offset_of_reflectionBlur_6() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___reflectionBlur_6)); }
	inline float get_reflectionBlur_6() const { return ___reflectionBlur_6; }
	inline float* get_address_of_reflectionBlur_6() { return &___reflectionBlur_6; }
	inline void set_reflectionBlur_6(float value)
	{
		___reflectionBlur_6 = value;
	}

	inline static int32_t get_offset_of_reflectBackfaces_7() { return static_cast<int32_t>(offsetof(ReflectionSettings_t282755190, ___reflectBackfaces_7)); }
	inline bool get_reflectBackfaces_7() const { return ___reflectBackfaces_7; }
	inline bool* get_address_of_reflectBackfaces_7() { return &___reflectBackfaces_7; }
	inline void set_reflectBackfaces_7(bool value)
	{
		___reflectBackfaces_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
struct ReflectionSettings_t282755190_marshaled_pinvoke
{
	int32_t ___blendType_0;
	int32_t ___reflectionQuality_1;
	float ___maxDistance_2;
	int32_t ___iterationCount_3;
	int32_t ___stepSize_4;
	float ___widthModifier_5;
	float ___reflectionBlur_6;
	int32_t ___reflectBackfaces_7;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings
struct ReflectionSettings_t282755190_marshaled_com
{
	int32_t ___blendType_0;
	int32_t ___reflectionQuality_1;
	float ___maxDistance_2;
	int32_t ___iterationCount_3;
	int32_t ___stepSize_4;
	float ___widthModifier_5;
	float ___reflectionBlur_6;
	int32_t ___reflectBackfaces_7;
};
#endif // REFLECTIONSETTINGS_T282755190_H
#ifndef SETTINGS_T2874244444_H
#define SETTINGS_T2874244444_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationModel/Settings
struct  Settings_t2874244444 
{
public:
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::lowPercent
	float ___lowPercent_0;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::highPercent
	float ___highPercent_1;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::minLuminance
	float ___minLuminance_2;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::maxLuminance
	float ___maxLuminance_3;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::keyValue
	float ___keyValue_4;
	// System.Boolean UnityEngine.PostProcessing.EyeAdaptationModel/Settings::dynamicKeyValue
	bool ___dynamicKeyValue_5;
	// UnityEngine.PostProcessing.EyeAdaptationModel/EyeAdaptationType UnityEngine.PostProcessing.EyeAdaptationModel/Settings::adaptationType
	int32_t ___adaptationType_6;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::speedUp
	float ___speedUp_7;
	// System.Single UnityEngine.PostProcessing.EyeAdaptationModel/Settings::speedDown
	float ___speedDown_8;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationModel/Settings::logMin
	int32_t ___logMin_9;
	// System.Int32 UnityEngine.PostProcessing.EyeAdaptationModel/Settings::logMax
	int32_t ___logMax_10;

public:
	inline static int32_t get_offset_of_lowPercent_0() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___lowPercent_0)); }
	inline float get_lowPercent_0() const { return ___lowPercent_0; }
	inline float* get_address_of_lowPercent_0() { return &___lowPercent_0; }
	inline void set_lowPercent_0(float value)
	{
		___lowPercent_0 = value;
	}

	inline static int32_t get_offset_of_highPercent_1() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___highPercent_1)); }
	inline float get_highPercent_1() const { return ___highPercent_1; }
	inline float* get_address_of_highPercent_1() { return &___highPercent_1; }
	inline void set_highPercent_1(float value)
	{
		___highPercent_1 = value;
	}

	inline static int32_t get_offset_of_minLuminance_2() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___minLuminance_2)); }
	inline float get_minLuminance_2() const { return ___minLuminance_2; }
	inline float* get_address_of_minLuminance_2() { return &___minLuminance_2; }
	inline void set_minLuminance_2(float value)
	{
		___minLuminance_2 = value;
	}

	inline static int32_t get_offset_of_maxLuminance_3() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___maxLuminance_3)); }
	inline float get_maxLuminance_3() const { return ___maxLuminance_3; }
	inline float* get_address_of_maxLuminance_3() { return &___maxLuminance_3; }
	inline void set_maxLuminance_3(float value)
	{
		___maxLuminance_3 = value;
	}

	inline static int32_t get_offset_of_keyValue_4() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___keyValue_4)); }
	inline float get_keyValue_4() const { return ___keyValue_4; }
	inline float* get_address_of_keyValue_4() { return &___keyValue_4; }
	inline void set_keyValue_4(float value)
	{
		___keyValue_4 = value;
	}

	inline static int32_t get_offset_of_dynamicKeyValue_5() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___dynamicKeyValue_5)); }
	inline bool get_dynamicKeyValue_5() const { return ___dynamicKeyValue_5; }
	inline bool* get_address_of_dynamicKeyValue_5() { return &___dynamicKeyValue_5; }
	inline void set_dynamicKeyValue_5(bool value)
	{
		___dynamicKeyValue_5 = value;
	}

	inline static int32_t get_offset_of_adaptationType_6() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___adaptationType_6)); }
	inline int32_t get_adaptationType_6() const { return ___adaptationType_6; }
	inline int32_t* get_address_of_adaptationType_6() { return &___adaptationType_6; }
	inline void set_adaptationType_6(int32_t value)
	{
		___adaptationType_6 = value;
	}

	inline static int32_t get_offset_of_speedUp_7() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___speedUp_7)); }
	inline float get_speedUp_7() const { return ___speedUp_7; }
	inline float* get_address_of_speedUp_7() { return &___speedUp_7; }
	inline void set_speedUp_7(float value)
	{
		___speedUp_7 = value;
	}

	inline static int32_t get_offset_of_speedDown_8() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___speedDown_8)); }
	inline float get_speedDown_8() const { return ___speedDown_8; }
	inline float* get_address_of_speedDown_8() { return &___speedDown_8; }
	inline void set_speedDown_8(float value)
	{
		___speedDown_8 = value;
	}

	inline static int32_t get_offset_of_logMin_9() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___logMin_9)); }
	inline int32_t get_logMin_9() const { return ___logMin_9; }
	inline int32_t* get_address_of_logMin_9() { return &___logMin_9; }
	inline void set_logMin_9(int32_t value)
	{
		___logMin_9 = value;
	}

	inline static int32_t get_offset_of_logMax_10() { return static_cast<int32_t>(offsetof(Settings_t2874244444, ___logMax_10)); }
	inline int32_t get_logMax_10() const { return ___logMax_10; }
	inline int32_t* get_address_of_logMax_10() { return &___logMax_10; }
	inline void set_logMax_10(int32_t value)
	{
		___logMax_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.EyeAdaptationModel/Settings
struct Settings_t2874244444_marshaled_pinvoke
{
	float ___lowPercent_0;
	float ___highPercent_1;
	float ___minLuminance_2;
	float ___maxLuminance_3;
	float ___keyValue_4;
	int32_t ___dynamicKeyValue_5;
	int32_t ___adaptationType_6;
	float ___speedUp_7;
	float ___speedDown_8;
	int32_t ___logMin_9;
	int32_t ___logMax_10;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.EyeAdaptationModel/Settings
struct Settings_t2874244444_marshaled_com
{
	float ___lowPercent_0;
	float ___highPercent_1;
	float ___minLuminance_2;
	float ___maxLuminance_3;
	float ___keyValue_4;
	int32_t ___dynamicKeyValue_5;
	int32_t ___adaptationType_6;
	float ___speedUp_7;
	float ___speedDown_8;
	int32_t ___logMin_9;
	int32_t ___logMax_10;
};
#endif // SETTINGS_T2874244444_H
#ifndef COLORWHEELSSETTINGS_T3120867866_H
#define COLORWHEELSSETTINGS_T3120867866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings
struct  ColorWheelsSettings_t3120867866 
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelMode UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::mode
	int32_t ___mode_0;
	// UnityEngine.PostProcessing.ColorGradingModel/LogWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::log
	LogWheelsSettings_t1545220311  ___log_1;
	// UnityEngine.PostProcessing.ColorGradingModel/LinearWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings::linear
	LinearWheelsSettings_t3897781309  ___linear_2;

public:
	inline static int32_t get_offset_of_mode_0() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t3120867866, ___mode_0)); }
	inline int32_t get_mode_0() const { return ___mode_0; }
	inline int32_t* get_address_of_mode_0() { return &___mode_0; }
	inline void set_mode_0(int32_t value)
	{
		___mode_0 = value;
	}

	inline static int32_t get_offset_of_log_1() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t3120867866, ___log_1)); }
	inline LogWheelsSettings_t1545220311  get_log_1() const { return ___log_1; }
	inline LogWheelsSettings_t1545220311 * get_address_of_log_1() { return &___log_1; }
	inline void set_log_1(LogWheelsSettings_t1545220311  value)
	{
		___log_1 = value;
	}

	inline static int32_t get_offset_of_linear_2() { return static_cast<int32_t>(offsetof(ColorWheelsSettings_t3120867866, ___linear_2)); }
	inline LinearWheelsSettings_t3897781309  get_linear_2() const { return ___linear_2; }
	inline LinearWheelsSettings_t3897781309 * get_address_of_linear_2() { return &___linear_2; }
	inline void set_linear_2(LinearWheelsSettings_t3897781309  value)
	{
		___linear_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORWHEELSSETTINGS_T3120867866_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef VIGNETTEMODEL_T2845517177_H
#define VIGNETTEMODEL_T2845517177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.VignetteModel
struct  VignetteModel_t2845517177  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.VignetteModel/Settings UnityEngine.PostProcessing.VignetteModel::m_Settings
	Settings_t1354494600  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(VignetteModel_t2845517177, ___m_Settings_1)); }
	inline Settings_t1354494600  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t1354494600 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t1354494600  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIGNETTEMODEL_T2845517177_H
#ifndef EYEADAPTATIONMODEL_T242823912_H
#define EYEADAPTATIONMODEL_T242823912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.EyeAdaptationModel
struct  EyeAdaptationModel_t242823912  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.EyeAdaptationModel/Settings UnityEngine.PostProcessing.EyeAdaptationModel::m_Settings
	Settings_t2874244444  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(EyeAdaptationModel_t242823912, ___m_Settings_1)); }
	inline Settings_t2874244444  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t2874244444 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t2874244444  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EYEADAPTATIONMODEL_T242823912_H
#ifndef POSTPROCESSINGPROFILE_T724195375_H
#define POSTPROCESSINGPROFILE_T724195375_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingProfile
struct  PostProcessingProfile_t724195375  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.PostProcessing.BuiltinDebugViewsModel UnityEngine.PostProcessing.PostProcessingProfile::debugViews
	BuiltinDebugViewsModel_t1462618840 * ___debugViews_2;
	// UnityEngine.PostProcessing.FogModel UnityEngine.PostProcessing.PostProcessingProfile::fog
	FogModel_t3620688749 * ___fog_3;
	// UnityEngine.PostProcessing.AntialiasingModel UnityEngine.PostProcessing.PostProcessingProfile::antialiasing
	AntialiasingModel_t1521139388 * ___antialiasing_4;
	// UnityEngine.PostProcessing.AmbientOcclusionModel UnityEngine.PostProcessing.PostProcessingProfile::ambientOcclusion
	AmbientOcclusionModel_t389471066 * ___ambientOcclusion_5;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel UnityEngine.PostProcessing.PostProcessingProfile::screenSpaceReflection
	ScreenSpaceReflectionModel_t3026344732 * ___screenSpaceReflection_6;
	// UnityEngine.PostProcessing.DepthOfFieldModel UnityEngine.PostProcessing.PostProcessingProfile::depthOfField
	DepthOfFieldModel_t514067330 * ___depthOfField_7;
	// UnityEngine.PostProcessing.MotionBlurModel UnityEngine.PostProcessing.PostProcessingProfile::motionBlur
	MotionBlurModel_t3080286123 * ___motionBlur_8;
	// UnityEngine.PostProcessing.EyeAdaptationModel UnityEngine.PostProcessing.PostProcessingProfile::eyeAdaptation
	EyeAdaptationModel_t242823912 * ___eyeAdaptation_9;
	// UnityEngine.PostProcessing.BloomModel UnityEngine.PostProcessing.PostProcessingProfile::bloom
	BloomModel_t2099727860 * ___bloom_10;
	// UnityEngine.PostProcessing.ColorGradingModel UnityEngine.PostProcessing.PostProcessingProfile::colorGrading
	ColorGradingModel_t1448048181 * ___colorGrading_11;
	// UnityEngine.PostProcessing.UserLutModel UnityEngine.PostProcessing.PostProcessingProfile::userLut
	UserLutModel_t1670108080 * ___userLut_12;
	// UnityEngine.PostProcessing.ChromaticAberrationModel UnityEngine.PostProcessing.PostProcessingProfile::chromaticAberration
	ChromaticAberrationModel_t3963399853 * ___chromaticAberration_13;
	// UnityEngine.PostProcessing.GrainModel UnityEngine.PostProcessing.PostProcessingProfile::grain
	GrainModel_t1152882488 * ___grain_14;
	// UnityEngine.PostProcessing.VignetteModel UnityEngine.PostProcessing.PostProcessingProfile::vignette
	VignetteModel_t2845517177 * ___vignette_15;
	// UnityEngine.PostProcessing.DitheringModel UnityEngine.PostProcessing.PostProcessingProfile::dithering
	DitheringModel_t2429005396 * ___dithering_16;

public:
	inline static int32_t get_offset_of_debugViews_2() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___debugViews_2)); }
	inline BuiltinDebugViewsModel_t1462618840 * get_debugViews_2() const { return ___debugViews_2; }
	inline BuiltinDebugViewsModel_t1462618840 ** get_address_of_debugViews_2() { return &___debugViews_2; }
	inline void set_debugViews_2(BuiltinDebugViewsModel_t1462618840 * value)
	{
		___debugViews_2 = value;
		Il2CppCodeGenWriteBarrier((&___debugViews_2), value);
	}

	inline static int32_t get_offset_of_fog_3() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___fog_3)); }
	inline FogModel_t3620688749 * get_fog_3() const { return ___fog_3; }
	inline FogModel_t3620688749 ** get_address_of_fog_3() { return &___fog_3; }
	inline void set_fog_3(FogModel_t3620688749 * value)
	{
		___fog_3 = value;
		Il2CppCodeGenWriteBarrier((&___fog_3), value);
	}

	inline static int32_t get_offset_of_antialiasing_4() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___antialiasing_4)); }
	inline AntialiasingModel_t1521139388 * get_antialiasing_4() const { return ___antialiasing_4; }
	inline AntialiasingModel_t1521139388 ** get_address_of_antialiasing_4() { return &___antialiasing_4; }
	inline void set_antialiasing_4(AntialiasingModel_t1521139388 * value)
	{
		___antialiasing_4 = value;
		Il2CppCodeGenWriteBarrier((&___antialiasing_4), value);
	}

	inline static int32_t get_offset_of_ambientOcclusion_5() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___ambientOcclusion_5)); }
	inline AmbientOcclusionModel_t389471066 * get_ambientOcclusion_5() const { return ___ambientOcclusion_5; }
	inline AmbientOcclusionModel_t389471066 ** get_address_of_ambientOcclusion_5() { return &___ambientOcclusion_5; }
	inline void set_ambientOcclusion_5(AmbientOcclusionModel_t389471066 * value)
	{
		___ambientOcclusion_5 = value;
		Il2CppCodeGenWriteBarrier((&___ambientOcclusion_5), value);
	}

	inline static int32_t get_offset_of_screenSpaceReflection_6() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___screenSpaceReflection_6)); }
	inline ScreenSpaceReflectionModel_t3026344732 * get_screenSpaceReflection_6() const { return ___screenSpaceReflection_6; }
	inline ScreenSpaceReflectionModel_t3026344732 ** get_address_of_screenSpaceReflection_6() { return &___screenSpaceReflection_6; }
	inline void set_screenSpaceReflection_6(ScreenSpaceReflectionModel_t3026344732 * value)
	{
		___screenSpaceReflection_6 = value;
		Il2CppCodeGenWriteBarrier((&___screenSpaceReflection_6), value);
	}

	inline static int32_t get_offset_of_depthOfField_7() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___depthOfField_7)); }
	inline DepthOfFieldModel_t514067330 * get_depthOfField_7() const { return ___depthOfField_7; }
	inline DepthOfFieldModel_t514067330 ** get_address_of_depthOfField_7() { return &___depthOfField_7; }
	inline void set_depthOfField_7(DepthOfFieldModel_t514067330 * value)
	{
		___depthOfField_7 = value;
		Il2CppCodeGenWriteBarrier((&___depthOfField_7), value);
	}

	inline static int32_t get_offset_of_motionBlur_8() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___motionBlur_8)); }
	inline MotionBlurModel_t3080286123 * get_motionBlur_8() const { return ___motionBlur_8; }
	inline MotionBlurModel_t3080286123 ** get_address_of_motionBlur_8() { return &___motionBlur_8; }
	inline void set_motionBlur_8(MotionBlurModel_t3080286123 * value)
	{
		___motionBlur_8 = value;
		Il2CppCodeGenWriteBarrier((&___motionBlur_8), value);
	}

	inline static int32_t get_offset_of_eyeAdaptation_9() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___eyeAdaptation_9)); }
	inline EyeAdaptationModel_t242823912 * get_eyeAdaptation_9() const { return ___eyeAdaptation_9; }
	inline EyeAdaptationModel_t242823912 ** get_address_of_eyeAdaptation_9() { return &___eyeAdaptation_9; }
	inline void set_eyeAdaptation_9(EyeAdaptationModel_t242823912 * value)
	{
		___eyeAdaptation_9 = value;
		Il2CppCodeGenWriteBarrier((&___eyeAdaptation_9), value);
	}

	inline static int32_t get_offset_of_bloom_10() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___bloom_10)); }
	inline BloomModel_t2099727860 * get_bloom_10() const { return ___bloom_10; }
	inline BloomModel_t2099727860 ** get_address_of_bloom_10() { return &___bloom_10; }
	inline void set_bloom_10(BloomModel_t2099727860 * value)
	{
		___bloom_10 = value;
		Il2CppCodeGenWriteBarrier((&___bloom_10), value);
	}

	inline static int32_t get_offset_of_colorGrading_11() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___colorGrading_11)); }
	inline ColorGradingModel_t1448048181 * get_colorGrading_11() const { return ___colorGrading_11; }
	inline ColorGradingModel_t1448048181 ** get_address_of_colorGrading_11() { return &___colorGrading_11; }
	inline void set_colorGrading_11(ColorGradingModel_t1448048181 * value)
	{
		___colorGrading_11 = value;
		Il2CppCodeGenWriteBarrier((&___colorGrading_11), value);
	}

	inline static int32_t get_offset_of_userLut_12() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___userLut_12)); }
	inline UserLutModel_t1670108080 * get_userLut_12() const { return ___userLut_12; }
	inline UserLutModel_t1670108080 ** get_address_of_userLut_12() { return &___userLut_12; }
	inline void set_userLut_12(UserLutModel_t1670108080 * value)
	{
		___userLut_12 = value;
		Il2CppCodeGenWriteBarrier((&___userLut_12), value);
	}

	inline static int32_t get_offset_of_chromaticAberration_13() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___chromaticAberration_13)); }
	inline ChromaticAberrationModel_t3963399853 * get_chromaticAberration_13() const { return ___chromaticAberration_13; }
	inline ChromaticAberrationModel_t3963399853 ** get_address_of_chromaticAberration_13() { return &___chromaticAberration_13; }
	inline void set_chromaticAberration_13(ChromaticAberrationModel_t3963399853 * value)
	{
		___chromaticAberration_13 = value;
		Il2CppCodeGenWriteBarrier((&___chromaticAberration_13), value);
	}

	inline static int32_t get_offset_of_grain_14() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___grain_14)); }
	inline GrainModel_t1152882488 * get_grain_14() const { return ___grain_14; }
	inline GrainModel_t1152882488 ** get_address_of_grain_14() { return &___grain_14; }
	inline void set_grain_14(GrainModel_t1152882488 * value)
	{
		___grain_14 = value;
		Il2CppCodeGenWriteBarrier((&___grain_14), value);
	}

	inline static int32_t get_offset_of_vignette_15() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___vignette_15)); }
	inline VignetteModel_t2845517177 * get_vignette_15() const { return ___vignette_15; }
	inline VignetteModel_t2845517177 ** get_address_of_vignette_15() { return &___vignette_15; }
	inline void set_vignette_15(VignetteModel_t2845517177 * value)
	{
		___vignette_15 = value;
		Il2CppCodeGenWriteBarrier((&___vignette_15), value);
	}

	inline static int32_t get_offset_of_dithering_16() { return static_cast<int32_t>(offsetof(PostProcessingProfile_t724195375, ___dithering_16)); }
	inline DitheringModel_t2429005396 * get_dithering_16() const { return ___dithering_16; }
	inline DitheringModel_t2429005396 ** get_address_of_dithering_16() { return &___dithering_16; }
	inline void set_dithering_16(DitheringModel_t2429005396 * value)
	{
		___dithering_16 = value;
		Il2CppCodeGenWriteBarrier((&___dithering_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGPROFILE_T724195375_H
#ifndef SETTINGS_T1995791524_H
#define SETTINGS_T1995791524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
struct  Settings_t1995791524 
{
public:
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ReflectionSettings UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings::reflection
	ReflectionSettings_t282755190  ___reflection_0;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/IntensitySettings UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings::intensity
	IntensitySettings_t1721872184  ___intensity_1;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/ScreenEdgeMask UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings::screenEdgeMask
	ScreenEdgeMask_t4063288584  ___screenEdgeMask_2;

public:
	inline static int32_t get_offset_of_reflection_0() { return static_cast<int32_t>(offsetof(Settings_t1995791524, ___reflection_0)); }
	inline ReflectionSettings_t282755190  get_reflection_0() const { return ___reflection_0; }
	inline ReflectionSettings_t282755190 * get_address_of_reflection_0() { return &___reflection_0; }
	inline void set_reflection_0(ReflectionSettings_t282755190  value)
	{
		___reflection_0 = value;
	}

	inline static int32_t get_offset_of_intensity_1() { return static_cast<int32_t>(offsetof(Settings_t1995791524, ___intensity_1)); }
	inline IntensitySettings_t1721872184  get_intensity_1() const { return ___intensity_1; }
	inline IntensitySettings_t1721872184 * get_address_of_intensity_1() { return &___intensity_1; }
	inline void set_intensity_1(IntensitySettings_t1721872184  value)
	{
		___intensity_1 = value;
	}

	inline static int32_t get_offset_of_screenEdgeMask_2() { return static_cast<int32_t>(offsetof(Settings_t1995791524, ___screenEdgeMask_2)); }
	inline ScreenEdgeMask_t4063288584  get_screenEdgeMask_2() const { return ___screenEdgeMask_2; }
	inline ScreenEdgeMask_t4063288584 * get_address_of_screenEdgeMask_2() { return &___screenEdgeMask_2; }
	inline void set_screenEdgeMask_2(ScreenEdgeMask_t4063288584  value)
	{
		___screenEdgeMask_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
struct Settings_t1995791524_marshaled_pinvoke
{
	ReflectionSettings_t282755190_marshaled_pinvoke ___reflection_0;
	IntensitySettings_t1721872184  ___intensity_1;
	ScreenEdgeMask_t4063288584  ___screenEdgeMask_2;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings
struct Settings_t1995791524_marshaled_com
{
	ReflectionSettings_t282755190_marshaled_com ___reflection_0;
	IntensitySettings_t1721872184  ___intensity_1;
	ScreenEdgeMask_t4063288584  ___screenEdgeMask_2;
};
#endif // SETTINGS_T1995791524_H
#ifndef SETTINGS_T451872061_H
#define SETTINGS_T451872061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ColorGradingModel/Settings
struct  Settings_t451872061 
{
public:
	// UnityEngine.PostProcessing.ColorGradingModel/TonemappingSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::tonemapping
	TonemappingSettings_t4154044775  ___tonemapping_0;
	// UnityEngine.PostProcessing.ColorGradingModel/BasicSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::basic
	BasicSettings_t838098426  ___basic_1;
	// UnityEngine.PostProcessing.ColorGradingModel/ChannelMixerSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::channelMixer
	ChannelMixerSettings_t898701698  ___channelMixer_2;
	// UnityEngine.PostProcessing.ColorGradingModel/ColorWheelsSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::colorWheels
	ColorWheelsSettings_t3120867866  ___colorWheels_3;
	// UnityEngine.PostProcessing.ColorGradingModel/CurvesSettings UnityEngine.PostProcessing.ColorGradingModel/Settings::curves
	CurvesSettings_t2830270037  ___curves_4;

public:
	inline static int32_t get_offset_of_tonemapping_0() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___tonemapping_0)); }
	inline TonemappingSettings_t4154044775  get_tonemapping_0() const { return ___tonemapping_0; }
	inline TonemappingSettings_t4154044775 * get_address_of_tonemapping_0() { return &___tonemapping_0; }
	inline void set_tonemapping_0(TonemappingSettings_t4154044775  value)
	{
		___tonemapping_0 = value;
	}

	inline static int32_t get_offset_of_basic_1() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___basic_1)); }
	inline BasicSettings_t838098426  get_basic_1() const { return ___basic_1; }
	inline BasicSettings_t838098426 * get_address_of_basic_1() { return &___basic_1; }
	inline void set_basic_1(BasicSettings_t838098426  value)
	{
		___basic_1 = value;
	}

	inline static int32_t get_offset_of_channelMixer_2() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___channelMixer_2)); }
	inline ChannelMixerSettings_t898701698  get_channelMixer_2() const { return ___channelMixer_2; }
	inline ChannelMixerSettings_t898701698 * get_address_of_channelMixer_2() { return &___channelMixer_2; }
	inline void set_channelMixer_2(ChannelMixerSettings_t898701698  value)
	{
		___channelMixer_2 = value;
	}

	inline static int32_t get_offset_of_colorWheels_3() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___colorWheels_3)); }
	inline ColorWheelsSettings_t3120867866  get_colorWheels_3() const { return ___colorWheels_3; }
	inline ColorWheelsSettings_t3120867866 * get_address_of_colorWheels_3() { return &___colorWheels_3; }
	inline void set_colorWheels_3(ColorWheelsSettings_t3120867866  value)
	{
		___colorWheels_3 = value;
	}

	inline static int32_t get_offset_of_curves_4() { return static_cast<int32_t>(offsetof(Settings_t451872061, ___curves_4)); }
	inline CurvesSettings_t2830270037  get_curves_4() const { return ___curves_4; }
	inline CurvesSettings_t2830270037 * get_address_of_curves_4() { return &___curves_4; }
	inline void set_curves_4(CurvesSettings_t2830270037  value)
	{
		___curves_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.PostProcessing.ColorGradingModel/Settings
struct Settings_t451872061_marshaled_pinvoke
{
	TonemappingSettings_t4154044775  ___tonemapping_0;
	BasicSettings_t838098426  ___basic_1;
	ChannelMixerSettings_t898701698  ___channelMixer_2;
	ColorWheelsSettings_t3120867866  ___colorWheels_3;
	CurvesSettings_t2830270037_marshaled_pinvoke ___curves_4;
};
// Native definition for COM marshalling of UnityEngine.PostProcessing.ColorGradingModel/Settings
struct Settings_t451872061_marshaled_com
{
	TonemappingSettings_t4154044775  ___tonemapping_0;
	BasicSettings_t838098426  ___basic_1;
	ChannelMixerSettings_t898701698  ___channelMixer_2;
	ColorWheelsSettings_t3120867866  ___colorWheels_3;
	CurvesSettings_t2830270037_marshaled_com ___curves_4;
};
#endif // SETTINGS_T451872061_H
#ifndef DEPTHOFFIELDMODEL_T514067330_H
#define DEPTHOFFIELDMODEL_T514067330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.DepthOfFieldModel
struct  DepthOfFieldModel_t514067330  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.DepthOfFieldModel/Settings UnityEngine.PostProcessing.DepthOfFieldModel::m_Settings
	Settings_t2195468135  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(DepthOfFieldModel_t514067330, ___m_Settings_1)); }
	inline Settings_t2195468135  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t2195468135 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t2195468135  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEPTHOFFIELDMODEL_T514067330_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SCREENSPACEREFLECTIONMODEL_T3026344732_H
#define SCREENSPACEREFLECTIONMODEL_T3026344732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.ScreenSpaceReflectionModel
struct  ScreenSpaceReflectionModel_t3026344732  : public PostProcessingModel_t540111976
{
public:
	// UnityEngine.PostProcessing.ScreenSpaceReflectionModel/Settings UnityEngine.PostProcessing.ScreenSpaceReflectionModel::m_Settings
	Settings_t1995791524  ___m_Settings_1;

public:
	inline static int32_t get_offset_of_m_Settings_1() { return static_cast<int32_t>(offsetof(ScreenSpaceReflectionModel_t3026344732, ___m_Settings_1)); }
	inline Settings_t1995791524  get_m_Settings_1() const { return ___m_Settings_1; }
	inline Settings_t1995791524 * get_address_of_m_Settings_1() { return &___m_Settings_1; }
	inline void set_m_Settings_1(Settings_t1995791524  value)
	{
		___m_Settings_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENSPACEREFLECTIONMODEL_T3026344732_H
#ifndef POSTPROCESSINGBEHAVIOUR_T3229946336_H
#define POSTPROCESSINGBEHAVIOUR_T3229946336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.PostProcessing.PostProcessingBehaviour
struct  PostProcessingBehaviour_t3229946336  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.PostProcessing.PostProcessingProfile UnityEngine.PostProcessing.PostProcessingBehaviour::profile
	PostProcessingProfile_t724195375 * ___profile_2;
	// System.Func`2<UnityEngine.Vector2,UnityEngine.Matrix4x4> UnityEngine.PostProcessing.PostProcessingBehaviour::jitteredMatrixFunc
	Func_2_t4093140010 * ___jitteredMatrixFunc_3;
	// System.Collections.Generic.Dictionary`2<System.Type,System.Collections.Generic.KeyValuePair`2<UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer>> UnityEngine.PostProcessing.PostProcessingBehaviour::m_CommandBuffers
	Dictionary_2_t1572824908 * ___m_CommandBuffers_4;
	// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase> UnityEngine.PostProcessing.PostProcessingBehaviour::m_Components
	List_1_t4203178569 * ___m_Components_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.PostProcessing.PostProcessingComponentBase,System.Boolean> UnityEngine.PostProcessing.PostProcessingBehaviour::m_ComponentStates
	Dictionary_2_t3095696878 * ___m_ComponentStates_6;
	// UnityEngine.PostProcessing.MaterialFactory UnityEngine.PostProcessing.PostProcessingBehaviour::m_MaterialFactory
	MaterialFactory_t2445948724 * ___m_MaterialFactory_7;
	// UnityEngine.PostProcessing.RenderTextureFactory UnityEngine.PostProcessing.PostProcessingBehaviour::m_RenderTextureFactory
	RenderTextureFactory_t1946967824 * ___m_RenderTextureFactory_8;
	// UnityEngine.PostProcessing.PostProcessingContext UnityEngine.PostProcessing.PostProcessingBehaviour::m_Context
	PostProcessingContext_t2014408948 * ___m_Context_9;
	// UnityEngine.Camera UnityEngine.PostProcessing.PostProcessingBehaviour::m_Camera
	Camera_t4157153871 * ___m_Camera_10;
	// UnityEngine.PostProcessing.PostProcessingProfile UnityEngine.PostProcessing.PostProcessingBehaviour::m_PreviousProfile
	PostProcessingProfile_t724195375 * ___m_PreviousProfile_11;
	// System.Boolean UnityEngine.PostProcessing.PostProcessingBehaviour::m_RenderingInSceneView
	bool ___m_RenderingInSceneView_12;
	// UnityEngine.PostProcessing.BuiltinDebugViewsComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_DebugViews
	BuiltinDebugViewsComponent_t2123147871 * ___m_DebugViews_13;
	// UnityEngine.PostProcessing.AmbientOcclusionComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_AmbientOcclusion
	AmbientOcclusionComponent_t4130625043 * ___m_AmbientOcclusion_14;
	// UnityEngine.PostProcessing.ScreenSpaceReflectionComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_ScreenSpaceReflection
	ScreenSpaceReflectionComponent_t856094247 * ___m_ScreenSpaceReflection_15;
	// UnityEngine.PostProcessing.FogComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_FogComponent
	FogComponent_t3400726830 * ___m_FogComponent_16;
	// UnityEngine.PostProcessing.MotionBlurComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_MotionBlur
	MotionBlurComponent_t3686516877 * ___m_MotionBlur_17;
	// UnityEngine.PostProcessing.TaaComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Taa
	TaaComponent_t3791749658 * ___m_Taa_18;
	// UnityEngine.PostProcessing.EyeAdaptationComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_EyeAdaptation
	EyeAdaptationComponent_t3394805121 * ___m_EyeAdaptation_19;
	// UnityEngine.PostProcessing.DepthOfFieldComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_DepthOfField
	DepthOfFieldComponent_t554756766 * ___m_DepthOfField_20;
	// UnityEngine.PostProcessing.BloomComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Bloom
	BloomComponent_t3791419130 * ___m_Bloom_21;
	// UnityEngine.PostProcessing.ChromaticAberrationComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_ChromaticAberration
	ChromaticAberrationComponent_t1647263118 * ___m_ChromaticAberration_22;
	// UnityEngine.PostProcessing.ColorGradingComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_ColorGrading
	ColorGradingComponent_t1715259467 * ___m_ColorGrading_23;
	// UnityEngine.PostProcessing.UserLutComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_UserLut
	UserLutComponent_t2843161776 * ___m_UserLut_24;
	// UnityEngine.PostProcessing.GrainComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Grain
	GrainComponent_t866324317 * ___m_Grain_25;
	// UnityEngine.PostProcessing.VignetteComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Vignette
	VignetteComponent_t3243642943 * ___m_Vignette_26;
	// UnityEngine.PostProcessing.DitheringComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Dithering
	DitheringComponent_t277621267 * ___m_Dithering_27;
	// UnityEngine.PostProcessing.FxaaComponent UnityEngine.PostProcessing.PostProcessingBehaviour::m_Fxaa
	FxaaComponent_t1312385771 * ___m_Fxaa_28;
	// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase> UnityEngine.PostProcessing.PostProcessingBehaviour::m_ComponentsToEnable
	List_1_t4203178569 * ___m_ComponentsToEnable_29;
	// System.Collections.Generic.List`1<UnityEngine.PostProcessing.PostProcessingComponentBase> UnityEngine.PostProcessing.PostProcessingBehaviour::m_ComponentsToDisable
	List_1_t4203178569 * ___m_ComponentsToDisable_30;

public:
	inline static int32_t get_offset_of_profile_2() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___profile_2)); }
	inline PostProcessingProfile_t724195375 * get_profile_2() const { return ___profile_2; }
	inline PostProcessingProfile_t724195375 ** get_address_of_profile_2() { return &___profile_2; }
	inline void set_profile_2(PostProcessingProfile_t724195375 * value)
	{
		___profile_2 = value;
		Il2CppCodeGenWriteBarrier((&___profile_2), value);
	}

	inline static int32_t get_offset_of_jitteredMatrixFunc_3() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___jitteredMatrixFunc_3)); }
	inline Func_2_t4093140010 * get_jitteredMatrixFunc_3() const { return ___jitteredMatrixFunc_3; }
	inline Func_2_t4093140010 ** get_address_of_jitteredMatrixFunc_3() { return &___jitteredMatrixFunc_3; }
	inline void set_jitteredMatrixFunc_3(Func_2_t4093140010 * value)
	{
		___jitteredMatrixFunc_3 = value;
		Il2CppCodeGenWriteBarrier((&___jitteredMatrixFunc_3), value);
	}

	inline static int32_t get_offset_of_m_CommandBuffers_4() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_CommandBuffers_4)); }
	inline Dictionary_2_t1572824908 * get_m_CommandBuffers_4() const { return ___m_CommandBuffers_4; }
	inline Dictionary_2_t1572824908 ** get_address_of_m_CommandBuffers_4() { return &___m_CommandBuffers_4; }
	inline void set_m_CommandBuffers_4(Dictionary_2_t1572824908 * value)
	{
		___m_CommandBuffers_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_CommandBuffers_4), value);
	}

	inline static int32_t get_offset_of_m_Components_5() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Components_5)); }
	inline List_1_t4203178569 * get_m_Components_5() const { return ___m_Components_5; }
	inline List_1_t4203178569 ** get_address_of_m_Components_5() { return &___m_Components_5; }
	inline void set_m_Components_5(List_1_t4203178569 * value)
	{
		___m_Components_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Components_5), value);
	}

	inline static int32_t get_offset_of_m_ComponentStates_6() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ComponentStates_6)); }
	inline Dictionary_2_t3095696878 * get_m_ComponentStates_6() const { return ___m_ComponentStates_6; }
	inline Dictionary_2_t3095696878 ** get_address_of_m_ComponentStates_6() { return &___m_ComponentStates_6; }
	inline void set_m_ComponentStates_6(Dictionary_2_t3095696878 * value)
	{
		___m_ComponentStates_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentStates_6), value);
	}

	inline static int32_t get_offset_of_m_MaterialFactory_7() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_MaterialFactory_7)); }
	inline MaterialFactory_t2445948724 * get_m_MaterialFactory_7() const { return ___m_MaterialFactory_7; }
	inline MaterialFactory_t2445948724 ** get_address_of_m_MaterialFactory_7() { return &___m_MaterialFactory_7; }
	inline void set_m_MaterialFactory_7(MaterialFactory_t2445948724 * value)
	{
		___m_MaterialFactory_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaterialFactory_7), value);
	}

	inline static int32_t get_offset_of_m_RenderTextureFactory_8() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_RenderTextureFactory_8)); }
	inline RenderTextureFactory_t1946967824 * get_m_RenderTextureFactory_8() const { return ___m_RenderTextureFactory_8; }
	inline RenderTextureFactory_t1946967824 ** get_address_of_m_RenderTextureFactory_8() { return &___m_RenderTextureFactory_8; }
	inline void set_m_RenderTextureFactory_8(RenderTextureFactory_t1946967824 * value)
	{
		___m_RenderTextureFactory_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_RenderTextureFactory_8), value);
	}

	inline static int32_t get_offset_of_m_Context_9() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Context_9)); }
	inline PostProcessingContext_t2014408948 * get_m_Context_9() const { return ___m_Context_9; }
	inline PostProcessingContext_t2014408948 ** get_address_of_m_Context_9() { return &___m_Context_9; }
	inline void set_m_Context_9(PostProcessingContext_t2014408948 * value)
	{
		___m_Context_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Context_9), value);
	}

	inline static int32_t get_offset_of_m_Camera_10() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Camera_10)); }
	inline Camera_t4157153871 * get_m_Camera_10() const { return ___m_Camera_10; }
	inline Camera_t4157153871 ** get_address_of_m_Camera_10() { return &___m_Camera_10; }
	inline void set_m_Camera_10(Camera_t4157153871 * value)
	{
		___m_Camera_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Camera_10), value);
	}

	inline static int32_t get_offset_of_m_PreviousProfile_11() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_PreviousProfile_11)); }
	inline PostProcessingProfile_t724195375 * get_m_PreviousProfile_11() const { return ___m_PreviousProfile_11; }
	inline PostProcessingProfile_t724195375 ** get_address_of_m_PreviousProfile_11() { return &___m_PreviousProfile_11; }
	inline void set_m_PreviousProfile_11(PostProcessingProfile_t724195375 * value)
	{
		___m_PreviousProfile_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_PreviousProfile_11), value);
	}

	inline static int32_t get_offset_of_m_RenderingInSceneView_12() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_RenderingInSceneView_12)); }
	inline bool get_m_RenderingInSceneView_12() const { return ___m_RenderingInSceneView_12; }
	inline bool* get_address_of_m_RenderingInSceneView_12() { return &___m_RenderingInSceneView_12; }
	inline void set_m_RenderingInSceneView_12(bool value)
	{
		___m_RenderingInSceneView_12 = value;
	}

	inline static int32_t get_offset_of_m_DebugViews_13() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_DebugViews_13)); }
	inline BuiltinDebugViewsComponent_t2123147871 * get_m_DebugViews_13() const { return ___m_DebugViews_13; }
	inline BuiltinDebugViewsComponent_t2123147871 ** get_address_of_m_DebugViews_13() { return &___m_DebugViews_13; }
	inline void set_m_DebugViews_13(BuiltinDebugViewsComponent_t2123147871 * value)
	{
		___m_DebugViews_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_DebugViews_13), value);
	}

	inline static int32_t get_offset_of_m_AmbientOcclusion_14() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_AmbientOcclusion_14)); }
	inline AmbientOcclusionComponent_t4130625043 * get_m_AmbientOcclusion_14() const { return ___m_AmbientOcclusion_14; }
	inline AmbientOcclusionComponent_t4130625043 ** get_address_of_m_AmbientOcclusion_14() { return &___m_AmbientOcclusion_14; }
	inline void set_m_AmbientOcclusion_14(AmbientOcclusionComponent_t4130625043 * value)
	{
		___m_AmbientOcclusion_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_AmbientOcclusion_14), value);
	}

	inline static int32_t get_offset_of_m_ScreenSpaceReflection_15() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ScreenSpaceReflection_15)); }
	inline ScreenSpaceReflectionComponent_t856094247 * get_m_ScreenSpaceReflection_15() const { return ___m_ScreenSpaceReflection_15; }
	inline ScreenSpaceReflectionComponent_t856094247 ** get_address_of_m_ScreenSpaceReflection_15() { return &___m_ScreenSpaceReflection_15; }
	inline void set_m_ScreenSpaceReflection_15(ScreenSpaceReflectionComponent_t856094247 * value)
	{
		___m_ScreenSpaceReflection_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScreenSpaceReflection_15), value);
	}

	inline static int32_t get_offset_of_m_FogComponent_16() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_FogComponent_16)); }
	inline FogComponent_t3400726830 * get_m_FogComponent_16() const { return ___m_FogComponent_16; }
	inline FogComponent_t3400726830 ** get_address_of_m_FogComponent_16() { return &___m_FogComponent_16; }
	inline void set_m_FogComponent_16(FogComponent_t3400726830 * value)
	{
		___m_FogComponent_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_FogComponent_16), value);
	}

	inline static int32_t get_offset_of_m_MotionBlur_17() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_MotionBlur_17)); }
	inline MotionBlurComponent_t3686516877 * get_m_MotionBlur_17() const { return ___m_MotionBlur_17; }
	inline MotionBlurComponent_t3686516877 ** get_address_of_m_MotionBlur_17() { return &___m_MotionBlur_17; }
	inline void set_m_MotionBlur_17(MotionBlurComponent_t3686516877 * value)
	{
		___m_MotionBlur_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_MotionBlur_17), value);
	}

	inline static int32_t get_offset_of_m_Taa_18() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Taa_18)); }
	inline TaaComponent_t3791749658 * get_m_Taa_18() const { return ___m_Taa_18; }
	inline TaaComponent_t3791749658 ** get_address_of_m_Taa_18() { return &___m_Taa_18; }
	inline void set_m_Taa_18(TaaComponent_t3791749658 * value)
	{
		___m_Taa_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Taa_18), value);
	}

	inline static int32_t get_offset_of_m_EyeAdaptation_19() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_EyeAdaptation_19)); }
	inline EyeAdaptationComponent_t3394805121 * get_m_EyeAdaptation_19() const { return ___m_EyeAdaptation_19; }
	inline EyeAdaptationComponent_t3394805121 ** get_address_of_m_EyeAdaptation_19() { return &___m_EyeAdaptation_19; }
	inline void set_m_EyeAdaptation_19(EyeAdaptationComponent_t3394805121 * value)
	{
		___m_EyeAdaptation_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_EyeAdaptation_19), value);
	}

	inline static int32_t get_offset_of_m_DepthOfField_20() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_DepthOfField_20)); }
	inline DepthOfFieldComponent_t554756766 * get_m_DepthOfField_20() const { return ___m_DepthOfField_20; }
	inline DepthOfFieldComponent_t554756766 ** get_address_of_m_DepthOfField_20() { return &___m_DepthOfField_20; }
	inline void set_m_DepthOfField_20(DepthOfFieldComponent_t554756766 * value)
	{
		___m_DepthOfField_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_DepthOfField_20), value);
	}

	inline static int32_t get_offset_of_m_Bloom_21() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Bloom_21)); }
	inline BloomComponent_t3791419130 * get_m_Bloom_21() const { return ___m_Bloom_21; }
	inline BloomComponent_t3791419130 ** get_address_of_m_Bloom_21() { return &___m_Bloom_21; }
	inline void set_m_Bloom_21(BloomComponent_t3791419130 * value)
	{
		___m_Bloom_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Bloom_21), value);
	}

	inline static int32_t get_offset_of_m_ChromaticAberration_22() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ChromaticAberration_22)); }
	inline ChromaticAberrationComponent_t1647263118 * get_m_ChromaticAberration_22() const { return ___m_ChromaticAberration_22; }
	inline ChromaticAberrationComponent_t1647263118 ** get_address_of_m_ChromaticAberration_22() { return &___m_ChromaticAberration_22; }
	inline void set_m_ChromaticAberration_22(ChromaticAberrationComponent_t1647263118 * value)
	{
		___m_ChromaticAberration_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_ChromaticAberration_22), value);
	}

	inline static int32_t get_offset_of_m_ColorGrading_23() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ColorGrading_23)); }
	inline ColorGradingComponent_t1715259467 * get_m_ColorGrading_23() const { return ___m_ColorGrading_23; }
	inline ColorGradingComponent_t1715259467 ** get_address_of_m_ColorGrading_23() { return &___m_ColorGrading_23; }
	inline void set_m_ColorGrading_23(ColorGradingComponent_t1715259467 * value)
	{
		___m_ColorGrading_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_ColorGrading_23), value);
	}

	inline static int32_t get_offset_of_m_UserLut_24() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_UserLut_24)); }
	inline UserLutComponent_t2843161776 * get_m_UserLut_24() const { return ___m_UserLut_24; }
	inline UserLutComponent_t2843161776 ** get_address_of_m_UserLut_24() { return &___m_UserLut_24; }
	inline void set_m_UserLut_24(UserLutComponent_t2843161776 * value)
	{
		___m_UserLut_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_UserLut_24), value);
	}

	inline static int32_t get_offset_of_m_Grain_25() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Grain_25)); }
	inline GrainComponent_t866324317 * get_m_Grain_25() const { return ___m_Grain_25; }
	inline GrainComponent_t866324317 ** get_address_of_m_Grain_25() { return &___m_Grain_25; }
	inline void set_m_Grain_25(GrainComponent_t866324317 * value)
	{
		___m_Grain_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_Grain_25), value);
	}

	inline static int32_t get_offset_of_m_Vignette_26() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Vignette_26)); }
	inline VignetteComponent_t3243642943 * get_m_Vignette_26() const { return ___m_Vignette_26; }
	inline VignetteComponent_t3243642943 ** get_address_of_m_Vignette_26() { return &___m_Vignette_26; }
	inline void set_m_Vignette_26(VignetteComponent_t3243642943 * value)
	{
		___m_Vignette_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Vignette_26), value);
	}

	inline static int32_t get_offset_of_m_Dithering_27() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Dithering_27)); }
	inline DitheringComponent_t277621267 * get_m_Dithering_27() const { return ___m_Dithering_27; }
	inline DitheringComponent_t277621267 ** get_address_of_m_Dithering_27() { return &___m_Dithering_27; }
	inline void set_m_Dithering_27(DitheringComponent_t277621267 * value)
	{
		___m_Dithering_27 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dithering_27), value);
	}

	inline static int32_t get_offset_of_m_Fxaa_28() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_Fxaa_28)); }
	inline FxaaComponent_t1312385771 * get_m_Fxaa_28() const { return ___m_Fxaa_28; }
	inline FxaaComponent_t1312385771 ** get_address_of_m_Fxaa_28() { return &___m_Fxaa_28; }
	inline void set_m_Fxaa_28(FxaaComponent_t1312385771 * value)
	{
		___m_Fxaa_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fxaa_28), value);
	}

	inline static int32_t get_offset_of_m_ComponentsToEnable_29() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ComponentsToEnable_29)); }
	inline List_1_t4203178569 * get_m_ComponentsToEnable_29() const { return ___m_ComponentsToEnable_29; }
	inline List_1_t4203178569 ** get_address_of_m_ComponentsToEnable_29() { return &___m_ComponentsToEnable_29; }
	inline void set_m_ComponentsToEnable_29(List_1_t4203178569 * value)
	{
		___m_ComponentsToEnable_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentsToEnable_29), value);
	}

	inline static int32_t get_offset_of_m_ComponentsToDisable_30() { return static_cast<int32_t>(offsetof(PostProcessingBehaviour_t3229946336, ___m_ComponentsToDisable_30)); }
	inline List_1_t4203178569 * get_m_ComponentsToDisable_30() const { return ___m_ComponentsToDisable_30; }
	inline List_1_t4203178569 ** get_address_of_m_ComponentsToDisable_30() { return &___m_ComponentsToDisable_30; }
	inline void set_m_ComponentsToDisable_30(List_1_t4203178569 * value)
	{
		___m_ComponentsToDisable_30 = value;
		Il2CppCodeGenWriteBarrier((&___m_ComponentsToDisable_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSTPROCESSINGBEHAVIOUR_T3229946336_H
#ifndef EXAMPLEWHEELCONTROLLER_T197115271_H
#define EXAMPLEWHEELCONTROLLER_T197115271_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ExampleWheelController
struct  ExampleWheelController_t197115271  : public MonoBehaviour_t3962482529
{
public:
	// System.Single ExampleWheelController::acceleration
	float ___acceleration_2;
	// UnityEngine.Renderer ExampleWheelController::motionVectorRenderer
	Renderer_t2627027031 * ___motionVectorRenderer_3;
	// UnityEngine.Rigidbody ExampleWheelController::m_Rigidbody
	Rigidbody_t3916780224 * ___m_Rigidbody_4;

public:
	inline static int32_t get_offset_of_acceleration_2() { return static_cast<int32_t>(offsetof(ExampleWheelController_t197115271, ___acceleration_2)); }
	inline float get_acceleration_2() const { return ___acceleration_2; }
	inline float* get_address_of_acceleration_2() { return &___acceleration_2; }
	inline void set_acceleration_2(float value)
	{
		___acceleration_2 = value;
	}

	inline static int32_t get_offset_of_motionVectorRenderer_3() { return static_cast<int32_t>(offsetof(ExampleWheelController_t197115271, ___motionVectorRenderer_3)); }
	inline Renderer_t2627027031 * get_motionVectorRenderer_3() const { return ___motionVectorRenderer_3; }
	inline Renderer_t2627027031 ** get_address_of_motionVectorRenderer_3() { return &___motionVectorRenderer_3; }
	inline void set_motionVectorRenderer_3(Renderer_t2627027031 * value)
	{
		___motionVectorRenderer_3 = value;
		Il2CppCodeGenWriteBarrier((&___motionVectorRenderer_3), value);
	}

	inline static int32_t get_offset_of_m_Rigidbody_4() { return static_cast<int32_t>(offsetof(ExampleWheelController_t197115271, ___m_Rigidbody_4)); }
	inline Rigidbody_t3916780224 * get_m_Rigidbody_4() const { return ___m_Rigidbody_4; }
	inline Rigidbody_t3916780224 ** get_address_of_m_Rigidbody_4() { return &___m_Rigidbody_4; }
	inline void set_m_Rigidbody_4(Rigidbody_t3916780224 * value)
	{
		___m_Rigidbody_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rigidbody_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXAMPLEWHEELCONTROLLER_T197115271_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (ChannelMixerSettings_t898701698)+ sizeof (RuntimeObject), sizeof(ChannelMixerSettings_t898701698 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1900[4] = 
{
	ChannelMixerSettings_t898701698::get_offset_of_red_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelMixerSettings_t898701698::get_offset_of_green_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelMixerSettings_t898701698::get_offset_of_blue_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ChannelMixerSettings_t898701698::get_offset_of_currentEditingChannel_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (LogWheelsSettings_t1545220311)+ sizeof (RuntimeObject), sizeof(LogWheelsSettings_t1545220311 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1901[3] = 
{
	LogWheelsSettings_t1545220311::get_offset_of_slope_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogWheelsSettings_t1545220311::get_offset_of_power_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LogWheelsSettings_t1545220311::get_offset_of_offset_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (LinearWheelsSettings_t3897781309)+ sizeof (RuntimeObject), sizeof(LinearWheelsSettings_t3897781309 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1902[3] = 
{
	LinearWheelsSettings_t3897781309::get_offset_of_lift_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LinearWheelsSettings_t3897781309::get_offset_of_gamma_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LinearWheelsSettings_t3897781309::get_offset_of_gain_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (ColorWheelMode_t1939415375)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1903[3] = 
{
	ColorWheelMode_t1939415375::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (ColorWheelsSettings_t3120867866)+ sizeof (RuntimeObject), sizeof(ColorWheelsSettings_t3120867866 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1904[3] = 
{
	ColorWheelsSettings_t3120867866::get_offset_of_mode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorWheelsSettings_t3120867866::get_offset_of_log_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ColorWheelsSettings_t3120867866::get_offset_of_linear_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (CurvesSettings_t2830270037)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[13] = 
{
	CurvesSettings_t2830270037::get_offset_of_master_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_red_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_green_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_blue_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_hueVShue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_hueVSsat_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_satVSsat_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_lumVSsat_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_e_CurrentEditingCurve_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_e_CurveY_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_e_CurveR_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_e_CurveG_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	CurvesSettings_t2830270037::get_offset_of_e_CurveB_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (Settings_t451872061)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[5] = 
{
	Settings_t451872061::get_offset_of_tonemapping_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t451872061::get_offset_of_basic_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t451872061::get_offset_of_channelMixer_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t451872061::get_offset_of_colorWheels_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t451872061::get_offset_of_curves_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (DepthOfFieldModel_t514067330), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[1] = 
{
	DepthOfFieldModel_t514067330::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (KernelSize_t2406218613)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1908[5] = 
{
	KernelSize_t2406218613::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (Settings_t2195468135)+ sizeof (RuntimeObject), sizeof(Settings_t2195468135_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1909[5] = 
{
	Settings_t2195468135::get_offset_of_focusDistance_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2195468135::get_offset_of_aperture_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2195468135::get_offset_of_focalLength_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2195468135::get_offset_of_useCameraFov_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2195468135::get_offset_of_kernelSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (DitheringModel_t2429005396), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1910[1] = 
{
	DitheringModel_t2429005396::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (Settings_t2313396630)+ sizeof (RuntimeObject), sizeof(Settings_t2313396630 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (EyeAdaptationModel_t242823912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[1] = 
{
	EyeAdaptationModel_t242823912::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (EyeAdaptationType_t3053468307)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1913[3] = 
{
	EyeAdaptationType_t3053468307::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (Settings_t2874244444)+ sizeof (RuntimeObject), sizeof(Settings_t2874244444_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1914[11] = 
{
	Settings_t2874244444::get_offset_of_lowPercent_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_highPercent_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_minLuminance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_maxLuminance_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_keyValue_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_dynamicKeyValue_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_adaptationType_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_speedUp_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_speedDown_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_logMin_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t2874244444::get_offset_of_logMax_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (FogModel_t3620688749), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1915[1] = 
{
	FogModel_t3620688749::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (Settings_t224798599)+ sizeof (RuntimeObject), sizeof(Settings_t224798599_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1916[1] = 
{
	Settings_t224798599::get_offset_of_excludeSkybox_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (GrainModel_t1152882488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1917[1] = 
{
	GrainModel_t1152882488::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (Settings_t4123292438)+ sizeof (RuntimeObject), sizeof(Settings_t4123292438_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1918[4] = 
{
	Settings_t4123292438::get_offset_of_colored_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t4123292438::get_offset_of_intensity_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t4123292438::get_offset_of_size_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t4123292438::get_offset_of_luminanceContribution_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (MotionBlurModel_t3080286123), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1919[1] = 
{
	MotionBlurModel_t3080286123::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (Settings_t4282162361)+ sizeof (RuntimeObject), sizeof(Settings_t4282162361 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1920[3] = 
{
	Settings_t4282162361::get_offset_of_shutterAngle_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t4282162361::get_offset_of_sampleCount_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t4282162361::get_offset_of_frameBlending_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1921 = { sizeof (ScreenSpaceReflectionModel_t3026344732), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1921[1] = 
{
	ScreenSpaceReflectionModel_t3026344732::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1922 = { sizeof (SSRResolution_t161222554)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1922[3] = 
{
	SSRResolution_t161222554::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1923 = { sizeof (SSRReflectionBlendType_t3026770880)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1923[3] = 
{
	SSRReflectionBlendType_t3026770880::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1924 = { sizeof (IntensitySettings_t1721872184)+ sizeof (RuntimeObject), sizeof(IntensitySettings_t1721872184 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1924[4] = 
{
	IntensitySettings_t1721872184::get_offset_of_reflectionMultiplier_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntensitySettings_t1721872184::get_offset_of_fadeDistance_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntensitySettings_t1721872184::get_offset_of_fresnelFade_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	IntensitySettings_t1721872184::get_offset_of_fresnelFadePower_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1925 = { sizeof (ReflectionSettings_t282755190)+ sizeof (RuntimeObject), sizeof(ReflectionSettings_t282755190_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1925[8] = 
{
	ReflectionSettings_t282755190::get_offset_of_blendType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_reflectionQuality_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_maxDistance_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_iterationCount_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_stepSize_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_widthModifier_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_reflectionBlur_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ReflectionSettings_t282755190::get_offset_of_reflectBackfaces_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1926 = { sizeof (ScreenEdgeMask_t4063288584)+ sizeof (RuntimeObject), sizeof(ScreenEdgeMask_t4063288584 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1926[1] = 
{
	ScreenEdgeMask_t4063288584::get_offset_of_intensity_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1927 = { sizeof (Settings_t1995791524)+ sizeof (RuntimeObject), sizeof(Settings_t1995791524_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1927[3] = 
{
	Settings_t1995791524::get_offset_of_reflection_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1995791524::get_offset_of_intensity_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1995791524::get_offset_of_screenEdgeMask_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1928 = { sizeof (UserLutModel_t1670108080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1928[1] = 
{
	UserLutModel_t1670108080::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1929 = { sizeof (Settings_t3006579223)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1929[2] = 
{
	Settings_t3006579223::get_offset_of_lut_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t3006579223::get_offset_of_contribution_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1930 = { sizeof (VignetteModel_t2845517177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1930[1] = 
{
	VignetteModel_t2845517177::get_offset_of_m_Settings_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1931 = { sizeof (Mode_t3936508933)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1931[3] = 
{
	Mode_t3936508933::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1932 = { sizeof (Settings_t1354494600)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1932[9] = 
{
	Settings_t1354494600::get_offset_of_mode_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_color_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_center_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_intensity_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_smoothness_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_roundness_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_mask_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_opacity_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Settings_t1354494600::get_offset_of_rounded_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1933 = { sizeof (PostProcessingBehaviour_t3229946336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1933[29] = 
{
	PostProcessingBehaviour_t3229946336::get_offset_of_profile_2(),
	PostProcessingBehaviour_t3229946336::get_offset_of_jitteredMatrixFunc_3(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_CommandBuffers_4(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Components_5(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_ComponentStates_6(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_MaterialFactory_7(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_RenderTextureFactory_8(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Context_9(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Camera_10(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_PreviousProfile_11(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_RenderingInSceneView_12(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_DebugViews_13(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_AmbientOcclusion_14(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_ScreenSpaceReflection_15(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_FogComponent_16(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_MotionBlur_17(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Taa_18(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_EyeAdaptation_19(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_DepthOfField_20(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Bloom_21(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_ChromaticAberration_22(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_ColorGrading_23(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_UserLut_24(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Grain_25(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Vignette_26(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Dithering_27(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_Fxaa_28(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_ComponentsToEnable_29(),
	PostProcessingBehaviour_t3229946336::get_offset_of_m_ComponentsToDisable_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1934 = { sizeof (PostProcessingComponentBase_t2731103827), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1934[1] = 
{
	PostProcessingComponentBase_t2731103827::get_offset_of_context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1935 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1935[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1936 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1937 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1938 = { sizeof (PostProcessingContext_t2014408948), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1938[5] = 
{
	PostProcessingContext_t2014408948::get_offset_of_profile_0(),
	PostProcessingContext_t2014408948::get_offset_of_camera_1(),
	PostProcessingContext_t2014408948::get_offset_of_materialFactory_2(),
	PostProcessingContext_t2014408948::get_offset_of_renderTextureFactory_3(),
	PostProcessingContext_t2014408948::get_offset_of_U3CinterruptedU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1939 = { sizeof (PostProcessingModel_t540111976), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1939[1] = 
{
	PostProcessingModel_t540111976::get_offset_of_m_Enabled_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1940 = { sizeof (PostProcessingProfile_t724195375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1940[15] = 
{
	PostProcessingProfile_t724195375::get_offset_of_debugViews_2(),
	PostProcessingProfile_t724195375::get_offset_of_fog_3(),
	PostProcessingProfile_t724195375::get_offset_of_antialiasing_4(),
	PostProcessingProfile_t724195375::get_offset_of_ambientOcclusion_5(),
	PostProcessingProfile_t724195375::get_offset_of_screenSpaceReflection_6(),
	PostProcessingProfile_t724195375::get_offset_of_depthOfField_7(),
	PostProcessingProfile_t724195375::get_offset_of_motionBlur_8(),
	PostProcessingProfile_t724195375::get_offset_of_eyeAdaptation_9(),
	PostProcessingProfile_t724195375::get_offset_of_bloom_10(),
	PostProcessingProfile_t724195375::get_offset_of_colorGrading_11(),
	PostProcessingProfile_t724195375::get_offset_of_userLut_12(),
	PostProcessingProfile_t724195375::get_offset_of_chromaticAberration_13(),
	PostProcessingProfile_t724195375::get_offset_of_grain_14(),
	PostProcessingProfile_t724195375::get_offset_of_vignette_15(),
	PostProcessingProfile_t724195375::get_offset_of_dithering_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1941 = { sizeof (ColorGradingCurve_t2000571184), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1941[5] = 
{
	ColorGradingCurve_t2000571184::get_offset_of_curve_0(),
	ColorGradingCurve_t2000571184::get_offset_of_m_Loop_1(),
	ColorGradingCurve_t2000571184::get_offset_of_m_ZeroValue_2(),
	ColorGradingCurve_t2000571184::get_offset_of_m_Range_3(),
	ColorGradingCurve_t2000571184::get_offset_of_m_InternalLoopingCurve_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1942 = { sizeof (GraphicsUtils_t2852986763), -1, sizeof(GraphicsUtils_t2852986763_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1942[2] = 
{
	GraphicsUtils_t2852986763_StaticFields::get_offset_of_s_WhiteTexture_0(),
	GraphicsUtils_t2852986763_StaticFields::get_offset_of_s_Quad_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1943 = { sizeof (MaterialFactory_t2445948724), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1943[1] = 
{
	MaterialFactory_t2445948724::get_offset_of_m_Materials_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1944 = { sizeof (RenderTextureFactory_t1946967824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1944[1] = 
{
	RenderTextureFactory_t1946967824::get_offset_of_m_TemporaryRTs_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1945 = { sizeof (ExampleWheelController_t197115271), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1945[3] = 
{
	ExampleWheelController_t197115271::get_offset_of_acceleration_2(),
	ExampleWheelController_t197115271::get_offset_of_motionVectorRenderer_3(),
	ExampleWheelController_t197115271::get_offset_of_m_Rigidbody_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1946 = { sizeof (Uniforms_t1233092826), -1, sizeof(Uniforms_t1233092826_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1946[1] = 
{
	Uniforms_t1233092826_StaticFields::get_offset_of__MotionAmount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1947 = { sizeof (U3CPrivateImplementationDetailsU3E_t3057255366), -1, sizeof(U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1947[2] = 
{
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2D51A7A390CD6DE245186881400B18C9D822EFE240_0(),
	U3CPrivateImplementationDetailsU3E_t3057255366_StaticFields::get_offset_of_U24fieldU2DC90F38A020811481753795774EB5AF353F414C59_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1948 = { sizeof (U24ArrayTypeU3D12_t2488454197)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D12_t2488454197 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1949 = { sizeof (U24ArrayTypeU3D24_t2467506693)+ sizeof (RuntimeObject), sizeof(U24ArrayTypeU3D24_t2467506693 ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
