﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour {

    private float _speedy = 5f;
    [SerializeField]
    public GameObject _enemyPrefab;
    
    [SerializeField]
    public GameObject _enemyExplosionPrefab;

    [SerializeField]
    private AudioClip _clip;

    private UIManager _uiManager;

    // Use this for initialization
    void Start () {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();       
	}
	
	// Update is called once per frame
	void Update () {
        Moviment();
    }

    private void Moviment()
    {
        transform.Translate(Vector3.down * _speedy * Time.deltaTime);

        //limite da tela, quando atingir o limite da tela e n for destruido ele retilza o objeto
        if (transform.position.y < -7)
        {
            transform.position = new Vector3(Random.Range(-3f, 3f), 6, 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.tag)
        {
            case "Player":PlayerCollider(other);
                break;
            case "Laser":
                LaserCollider(other);
                break;
            default:
                break;
        }        
    }

    private void PlayerCollider(Collider2D other)
    {
        Player player = other.GetComponent<Player>();
        if (player != null)
        {
            player.Demage(1);
            Destroy(this.gameObject);
            DestroyEnemy(5);
        }
    }

    private void LaserCollider(Collider2D other)
    {
        Laser laser = other.GetComponent<Laser>();
        if (laser != null)
        {
            //Verificando se o laser tem pai (no caso de trippleShoot)
            if (other.transform.parent != null)
                Destroy(other.transform.parent.gameObject);

            Destroy(other.gameObject);
            DestroyEnemy(10);
        }
    }

    public void DestroyEnemy(int value)
    {
        //TODO isso aqui pode haver concorrencia
        Destroy(this.gameObject);
        _uiManager.UpdateScore(value);

        Vector3 position = transform.position;
        Instantiate(_enemyExplosionPrefab, position, Quaternion.identity);
        AudioSource.PlayClipAtPoint(_clip, Camera.main.transform.position);
    }   
}
