﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public bool statePlay;

    [SerializeField]
    private GameObject _player;

    private UIManager _uiManager;

    [SerializeField]
    private GameObject _spawnManager;

	// Use this for initialization
	void Start () {
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
	}
	
	// Update is called once per frame
	void Update () {
        if (statePlay)
            return;

        if (Input.GetKey(KeyCode.Return) || Input.touchCount > 0)
        {
            statePlay = true;
            Instantiate(_player);
            Instantiate(_spawnManager);
            _uiManager.HideTitleScreem();
            _uiManager.ResetScore();            
        }
    }

    public void GameOver()
    {
        this.statePlay = false;       
        this._uiManager.ShowTitleScreem();
    }
}
