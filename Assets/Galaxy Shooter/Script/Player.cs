﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

    [SerializeField]
    private GameObject _laserPrefab;

    [SerializeField]
    private GameObject _tripleShootPrefab;

    [SerializeField]
    private GameObject _explosionPrefab;

    [SerializeField]
    private GameObject _sheildGameObject;

    [SerializeField]
    private float _fireRate = 0.25f;

    [SerializeField]
    private int life = 3;

    [SerializeField]
    private bool isSheildActive;

    [SerializeField]
    private bool _canEnableShield;

    private float _canFire = 0.0f;

    private AudioSource _audioSource;

    //AudioClip for Power ups
    [SerializeField]
    private AudioClip _clipPowerUp;

    public bool canTrippleShoot;
    public bool canSpeedUp;    

    
    private float _speed = 10f;   

    //Managers
    private UIManager _uiManager;
    private GameManager _gameManager;
    
    [SerializeField]
    private GameObject[] _engines;
    private int _hitCount;

    // Use this for initialization
    void Start() {
        //Pega a referecia dos componentes 
        _uiManager = GameObject.Find("Canvas").GetComponent<UIManager>();
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        //O componente AudioSource ja esta dentro da classe Player, entao eh so pegar a referencia
        _audioSource = GetComponent<AudioSource>();

        transform.position = new Vector3(0, 0, 0);

        if (_uiManager != null)
            _uiManager.UpdateLives(life);

        this._hitCount = 0;
    }

    // Update is called once per frame  0
    void Update() {
        Moviment();             

        if (Input.GetMouseButton(0) || Input.touchCount > 0)
        {           

            if (Time.time > _canFire)  
            {
                _audioSource.Play();
                if (canTrippleShoot)
                    tripleShoot();
                else
                    Shoot();

                _canFire = Time.time + _fireRate;
            }
        }

        if (Input.GetKeyDown(KeyCode.E)){
            if (this._canEnableShield) {
                ShieldPowerUp();              
            }
        }
    }

    private void Shoot()
    {       
        Instantiate(_laserPrefab, transform.position + new Vector3(0, 0.75f, 0), Quaternion.identity);
    }

    private void tripleShoot() {
        Instantiate(_tripleShootPrefab, transform.position, Quaternion.identity);
    }

    //movimente of player
    private void Moviment()
    {            

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0); // get first touch since touch count is greater than zero

            if (touch.phase == TouchPhase.Moved)
            {
                // get the touch position from the screen touch to world point
                Vector3 touchedPos = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x, touch.position.y, 10));
                Vector3 offSet = new Vector3(0, 1, 0);
                // lerp and set the position of the current object to that of the touch, but smoothly over time.
                transform.position = Vector3.Lerp(transform.position, touchedPos + offSet , Time.deltaTime * _speed);             
            }
        }


        //new vector3(1, 0, 0) * 5 * 0
        float horizontalAInput = Input.GetAxis("Horizontal");
        //if (this.canSpeedUp)
        //{
        //    float speedUp = _speed * 1.5f;
        //    transform.Translate(Vector3.right * speedUp * horizontalAInput * Time.deltaTime);
        //} else
        transform.Translate(Vector3.right * _speed * horizontalAInput * Time.deltaTime);


        //new vector3(0, 1, 0) * 5 * 0
        float VerticalInput = Input.GetAxis("Vertical");
        transform.Translate(Vector3.up * _speed * VerticalInput * Time.deltaTime);

        //transforme. position recebe uma nova posição do vetor3, para assim definir uma nova localização no espaço tempo do mapa
        if (transform.position.y > 4)
            transform.position = new Vector3(transform.position.x, 4, 0);
        else if (transform.position.y < -4.2f)
            transform.position = new Vector3(transform.position.x, -4.2f, 0);
        else if (transform.position.x > 3.12f)
            transform.position = new Vector3(3.12f, transform.position.y, 0);
        else if (transform.position.x < -3.12f)
            transform.position = new Vector3(-3.12f, transform.position.y, 0);
    }

    private void GerenareShootLaser(GameObject prefeb, Vector3 position)
    {
        Instantiate(prefeb, transform.position + position, Quaternion.identity);
    }

    public void TripleShotPowerUpOn()
    {
        PlayPowerUpSound();
       
        canTrippleShoot = true;
        StartCoroutine(TripleShotPowerDownRoutine());
    }

    public void SpeedUpPowerUpOn()
    {
        PlayPowerUpSound();
        this.canSpeedUp = true;
        StartCoroutine(SpeedDownRoutine());
    }

    public void ShieldPowerUp()
    {
        PlayPowerUpSound();
        this.setSheild(false);

        HabilitarShield(true);
        StartCoroutine(SheildPowerUpRoutine());       
    }    

    private IEnumerator SheildPowerUpRoutine()
    {
        yield return new WaitForSeconds(10.0f);
        HabilitarShield(false); 
    }

    private IEnumerator TripleShotPowerDownRoutine()
    {
        yield return new WaitForSeconds(5.0f);
        this.canTrippleShoot = false;
    }

    private IEnumerator SpeedDownRoutine()
    {
        this._fireRate = 0.05f;
        yield return new WaitForSeconds(5.0f);
        this._fireRate = 0.25f;
        this.canSpeedUp = false;
    }

    public void setSheild(bool value)
    {
        _uiManager.HasShield(value);
        this._canEnableShield = value;
    }

    public void Demage(int value)
    {
        if (isSheildActive) {
            HabilitarShield(false);
            return;
        }

        _hitCount++;
        if (_hitCount == 1)
            _engines[0].SetActive(true);
        else if (_hitCount == 2)
            _engines[1].SetActive(true);

        this.life = this.life - value;
        _uiManager.UpdateLives(life);

        if (this.life < 1)
        {                       
            Instantiate(_explosionPrefab, transform.position, Quaternion.identity);
            Destroy(this.gameObject);
            _gameManager.GameOver();
        }
    }

    private void HabilitarShield(bool status) {       
        this.isSheildActive = status;
        this._sheildGameObject.SetActive(status);
    }

    private void PlayPowerUpSound()
    {
        AudioSource.PlayClipAtPoint(_clipPowerUp, Camera.main.transform.position, 1f);
    }
}

