﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public Sprite[] lives;
    public Image livesImageDisplay;

    private int score;
    public Text scoreText;

    public GameObject titleScreem;
    public GameObject imageHasShield;


    void Start()
    {
        this.imageHasShield.SetActive(false);
    }  
    
    public void UpdateLives(int currentLive)
    {
        livesImageDisplay.sprite = lives[currentLive];
    }

    public void UpdateScore(int value) {
        score += value;
        scoreText.text = "Score: " + score.ToString();
    }

    public void HideTitleScreem()
    {
        this.titleScreem.SetActive(false);
    }

    public void ShowTitleScreem()
    {
        this.titleScreem.SetActive(true);
    }

    public void ResetScore()
    {
        this.score = 0;
    }

    public void HasShield(bool currentValue)
    {        
        this.imageHasShield.SetActive(currentValue);
    }

    
}
