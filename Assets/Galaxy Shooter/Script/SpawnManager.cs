﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    [SerializeField]
    private GameObject _enemyShipPrefab;

    [SerializeField]
    private GameObject[] _powerUps;

    private GameManager _gameManager;

    // [SerializeField]
    // private float _enemyRate = 25f;
    // private float _nextLevel = 0.0f;  
    // float dificult = 5f;

    // Use this for initialization
    void Start () {
        _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();

        //Os Courtatine estão  em um loop spawnand enemys e powerups
        StartCoroutine(EnemySpawnRoutine());
        StartCoroutine(PowerUpsSpawnRoutine());
	}

    private IEnumerator EnemySpawnRoutine()
    {
        while (_gameManager.statePlay)
        {           
            Instantiate(_enemyShipPrefab, RandomPosition(), Quaternion.identity);            
            yield return new WaitForSeconds(3);           
        }
    }

    private IEnumerator PowerUpsSpawnRoutine()
    {
        while (_gameManager.statePlay)
        {
            Instantiate(_powerUps[Random.Range(0, 3)], RandomPosition(), Quaternion.identity);
            yield return new WaitForSeconds(20f);
        }
    }

    private Vector3 RandomPosition()
    {
        Vector3 position = new Vector3(Random.Range(-3f, 3f), 6, 0);
        Debug.Log(position);
        return position;
    }
}
