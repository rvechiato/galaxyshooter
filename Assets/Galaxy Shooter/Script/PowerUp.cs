﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour {
     
    [SerializeField]
    private float _speed = 3.0f;

    [SerializeField]
    private int powerupID;
    
    // Update is called once per frame
    void Update () {
        transform.Translate(Vector3.down * _speed * Time.deltaTime);

        if (transform.position.y < -7)
        {
            Destroy(this.gameObject);
        }
	}

   private void OnTriggerEnter2D(Collider2D other)
   {        
        if (other.tag == "Player")
        {             
            Player player = other.GetComponent<Player>();
            if(player != null)
            {               
                switch (powerupID)
                {
                    case 0:
                        player.TripleShotPowerUpOn();                       
                        break;
                    case 1:
                        player.SpeedUpPowerUpOn();
                        break;
                    case 2:
                        player.setSheild(true);
                        break;
                    default:
                            break;
                }
               
                Destroy(this.gameObject);
            }
        }
    }
    
  
}
